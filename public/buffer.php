<?php
	require("../includes/config.php");
		
	// makes sure the page is called via get with parameter request
	if(!isset($_GET["request"]))
	{
		// generates permission denied error
        render("apology.php", ["title" => "it seems you are trying to access data you are not entitled to"]);
		return;
	}

	
	$info = send_request();

	// the page requested the title and artist info
	if($_GET["request"] == "info")
	{
		// checking helps avoid errors
		if(isset($info["byte"]) || isset($info["location"]))
		{
			unset($info["location"]);
		}
		// checks if there is no broadcast for now
		if($info == false)
		{
			echo("off");
		}
		else
		{	
			$img = temp_icon($info["img"]);
			// create an image if it does not exist already
			if($img == false)
			{
				$info["img"] = "none";
			}
			else
			{
				$info["img"] = $img;
			}
			// puts it in a Json Object and sends it
			$obj_info = json_encode($info, JSON_PRETTY_PRINT, 512);
			echo($obj_info);
		}
	}
	// The actual song data was requested
	else if($_GET["request"] == "buffer" && isset($_GET["offset"]))
	{
		if($info == false)
		{
			echo("off");
			return;
		}
		// sends a new segment
		$offset = intval($_GET["offset"]);

		$segment = create_buffer($info, $offset);
		if($segment == false)
		{
			echo("off");
		}
		else
		{
			echo($segment);
			sleep(30);
		}

	}
	// the request parameter was not set correctly
	else
	{
        render("apology.php", ["title" => "it seems you are trying to access data you are not entitled to"]);
	}
		
?>
