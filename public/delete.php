<?php
require("../includes/config.php");

if(isset($_GET['id']))
{
	$comment = mysql::query("SELECT `user_id` FROM `comments` where `id` = ? ", $_GET['id']);
	// checks if the user is trying to delete his own message 
	if($comment[0]["user_id"] == $_SESSION['id'])
	{
		mysql::query("DELETE FROM `comments` WHERE id = ?" , $_GET['id']);
		echo('ok');
	}
	else
	{
		render("error.php", ['title' => 'Cannot Delete comment', 'error' => 'It looks like you are trying to delete a comment that does not belong to this account']);
	}
}


?>