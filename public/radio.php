<?php
require("../includes/config.php");
if(!check_broadcast())
{
	render("apology.php", ["title" => "No Trasmission", "error" => "It appears that there is no broadcast at the moment. Please try again later or contact: abdullahem1997@hotmail.com"]);
}
else
{

	$js =  
	'
	<script src = "/js/radio.js"></script>
	<script src="/js/API/jquery-ui.min.js"></script>
	<script src="/js/API/socket.io.js"></script>
	<script src="/js/comments.js"></script>
	' ;
	$css = 
	'
		<link href="/css/comments.css" rel = "stylesheet" type = "text/css"/>
	';
	render("radio_interface.php", ["title" => "radio", "js" => $js, "css" => $css , "comments" => fetch_comments()]);	
}

?>
