<?php 
require("../includes/config.php");
// global
$css = 
'
<link href="/css/home-background.css" rel="stylesheet"/>
';
$js =  
	'
	<script src = "/js/authentication.js"></script>
	<script src = "/js/helpers.js"></script>
	' ;
if($_SERVER["REQUEST_METHOD"] == "GET")
{	
	render("login_form.php", ["title" => "Login-Sirens Radio", "js" => $js, "css" => $css]);
}
else if ($_SERVER["REQUEST_METHOD"] == "POST")
{
	// most probably the js was not working
	if(!isset($_POST["uname"]))
	{
		// checking the password and user name
		$hash = hash("md5",$_POST["password"]);
		$info = mysql::query("SELECT * FROM users WHERE username = ?" , $_POST["name"]);
		if(empty($info))
		{
			render('apology.php', ["title" => "Login failed", "error" => "The username or the password you just provided is incorrect. Please check your login information and try again."]);
		}
		else if($info[0]["hash"] != $hash)
		{
			render('apology.php', ["title" => "Login failed", "error" => "The username or the password you just provided is incorrect. Please check your login information and try again."]);
		}
		else
		{
			$js =  
				'
				<script src = "/js/radio.js"></script>
				<script src="/js/socket.io.js"></script>
				' ;
			$_SESSION["id"] = $info[0]["id"];
			render("radio_interface.php", ["title" => "radio", "js" => $js]);
		}
	}
	else
	{
		// checking the password and user name
		$hash = hash("md5",$_POST["password"]);
		$info = mysql::query("SELECT * FROM users WHERE username = ?" , $_POST["uname"]);
		if(empty($info))
		{
			print("Username was not found");
		}
		else if($info[0]["hash"] != $hash)
		{
			print("password is incorrect. Please Try again");
		}
		else
		{
			$_SESSION["id"] = $info[0]["id"];
			print("ok");
		}
	}
	
}

?>
