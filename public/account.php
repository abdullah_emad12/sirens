<?php 
require("../includes/config.php");

if($_SERVER['REQUEST_METHOD'] == 'GET')
{
	$js = '<script src = "/js/authentication.js"></script>
	<script src = "/js/helpers.js"></script>';
	$info = mysql::query("SELECT * FROM users WHERE id = ?" , $_SESSION['id']);
	if(sizeof($info) ==  0)
	{
		session_destroy();
		header("Location: index.php");
	}
	// renders the view with the username and full name
	render('account_form.php', ['title' => 'Your Account', 'js' => $js, 'username' => $info[0]['username'], 'fullname' => $info[0]['name']]);
}
else if($_SERVER['REQUEST_METHOD'] == 'POST')
{
	if(isset($_POST["password1"]) && isset($_POST["password2"]))
	{
			$hash = hash("md5",$_POST["password"]);
			$info = mysql::query("SELECT * FROM users WHERE id = ?" , $_SESSION['id']);
			// the query was returned successfully
			if(sizeof($info) > 0)
			{
				// password matches the database records
				if($hash != $info[0]["hash"])
				{
					render('apology.php', ["title" =>" Couldn't change passowrd", "error" => "The old Password you provided is incorrect !"]);
				}
			}
			// two passwords match
			if($_POST["password1"] != $_POST["password2"])
			{
				render('apology.php', ["title" =>" Couldn't change passowrd", "error" => "The two passwords that you provided does not match"]);
			}
			// password is more than 4 characters
			else if(strlen($_POST["password1"]) <= 4)
			{
				render('apology.php', ["title" =>" Couldn't change passowrd", "error" => "The password that you provided is too short"]);
			}
			else
			{
				// updates the user info
				$hash = hash("md5",$_POST["password1"]);
				$state = mysql::query("UPDATE users SET hash = ? WHERE id = ?",  $hash, $_SESSION['id']);
				header("location: radio.php");
			}
	}
	else
	{
		$hash = hash("md5",$_POST["Oldpassword"]);
		$info = mysql::query("SELECT * FROM users WHERE id = ?" , $_SESSION['id']);
		// the query was returned successfully
		// password matches the database records
		if($hash != $info[0]["hash"])
		{
			echo("You have provided an incorrect password");				
		
		}
		else
		{
			// updates the user info
			$hash = hash("md5",$_POST["Newpassword"]);
			$state = mysql::query("UPDATE users SET hash = ? WHERE id = ?",  $hash, $_SESSION['id']);
			if($state == true)
			{
				echo('ok');
			}
		}
	}
	
}


?>