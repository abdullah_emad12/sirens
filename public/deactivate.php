<?php
require("../includes/config.php");

// prompts the user for  the username and password f
if (!isset($_SERVER["PHP_AUTH_USER"]))
{
    header('WWW-Authenticate: Basic realm="My Realm"');
    header('HTTP/1.0 401 Unauthorized');

    // in case the user presses cancel
    echo("Don't try to leave us again :) click on the GIF to go the home page <br>");
	echo('<a href = "index.php"><img src = "http://image.blingee.com/images15/content/output/000/000/000/3ce/217891765_1286222.gif"/></a>');	
}
else 
{
	// gets the user information from the data base
	$hash = hash("md5",$_SERVER['PHP_AUTH_PW']);
	$info = mysql::query("SELECT * FROM users WHERE username = ?" , $_SERVER['PHP_AUTH_USER']);

	// the username was not found the db
	if(empty($info))
	{
		// prompts the user again
		header('WWW-Authenticate: Basic realm="My Realm"');
	    header('HTTP/1.0 401 Unauthorized');
	    echo('User name was not found...click on the GIF to go back <br>');

	    // user presses cancel
	    echo('<a href = "account.php"><img src = "http://68.media.tumblr.com/9f73e3f8a958dbe03b0f76f8811a54a1/tumblr_inline_mszgobnySp1qz4rgp.gif"/></a>');	
	}
	// compares the password with that of the db
	else if($hash != $info[0]["hash"])
	{
		// prompts the user again on incorrect password
		header('WWW-Authenticate: Basic realm="My Realm"');
	    header('HTTP/1.0 401 Unauthorized');
	    // the user presses cancel
	    echo('Incorrect Password... click on the GIF to go back<br>');
	    echo('<a href = "account.php"><img src = "http://68.media.tumblr.com/9f73e3f8a958dbe03b0f76f8811a54a1/tumblr_inline_mszgobnySp1qz4rgp.gif"/></a>');	
	}
	// makes sure the user is trying to deactivate the account he is currently logged with
	else if($info[0]['id'] != $_SESSION['id'])
	{
		header('WWW-Authenticate: Basic realm="My Realm"');
	    header('HTTP/1.0 401 Unauthorized');
	    echo('Incorrect Information.. click on the GIF to go back<br>');
	    echo('<a href = "account.php"><img src = "http://68.media.tumblr.com/9f73e3f8a958dbe03b0f76f8811a54a1/tumblr_inline_mszgobnySp1qz4rgp.gif"/></a>');	
	}
	else
	{
		// all the information given were correct
		mysql::query("DELETE FROM users WHERE username = ?" , $_SERVER['PHP_AUTH_USER']);// removes the user from the db
		echo('Your account is successfully deactivated! Click on the GIF<br>');
	    echo('<a href = "logout.php"><img src = "https://media.giphy.com/media/UQaRUOLveyjNC/giphy.gif"/></a>');
	    session_destroy();
	}	
}


?>