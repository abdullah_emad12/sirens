<?php
require("../includes/config.php");

// global

	
if($_SERVER["REQUEST_METHOD"] == "GET")
{
	// all the javascript to be included with this page
	$js =  
	'
	<script src = "/js/authentication.js"></script>
	<script src = "/js/helpers.js"></script>
	' ;
	render("register_form.php",["title" => "CS50 Radio - Register", "js" => $js]);
}
else if($_SERVER["REQUEST_METHOD"] == "POST")
{
	// in case the user does not have javascript
	if(!isset($_POST["fullname"]) || !isset($_POST["username"]) || !isset($_POST["hash"]) || !isset($_POST["sex"]))
	{
		// passwrds don't match
		if($_POST["password"] != $_POST["password2"])
		{
			render('apology.php', ["title" => "Registeration Error", "error" => "The two passwords you provided does not match each other. Make sure the two passwords are identical"]);
		}
		// password too short
		else if(strlen($_POST["password"]) < 5)
		{
			render('apology.php', ["title" => "Registeration Error", "error" => "Your password is two short. It must at least contain 5 characters"]);
		}
		// name too short
		else if(strlen($_POST["name"]) < 3)
		{
			render('apology.php', ["title" => "Registeration Error", "error" => "Your Full name does not appear to be legitamte, please make sure you type your real name"]);
		}
		//username too short
		else if(strlen($_POST["uname"]) < 4)
		{
			render('apology.php', ["title" => "Registeration Error", "error" => "Username must be at least 4 characters long, make sure you provide that"]);	
		}
		// gender not provided
		else if(!isset($_POST["gender"]))
		{
			
			render('apology.php', ["title" => "Registeration Error", "error" => "You have to provide a gender in order to procceed in the registeration process"]);
		}
		// username not available
		else
		{
			//prepares the data to be inserted in the database
			$hash = $_POST["password"];
			$hash = hash("md5",$hash);
			$state = mysql::query("INSERT INTO users( name, username, hash, gender ) VALUES (? , ? , ? , ?)", $_POST["name"], $_POST["uname"], $hash, $_POST["gender"]);
			// on failure, the username is probably taken
			if(!$state)
			{
				render('apology.php', ["title" => "Registeration Error", "error" => "The Username you have chose is already taken. Please pick another or Login if you are already registered"]);
			}
			else
			{
				$js =  
				'
				<script src = "/js/radio.js"></script>
				<script src="/js/socket.io.js"></script>
				<script src="/js/Queue.js"></script>
				' ;
				// on success logs in the user
				$info = mysql::query("SELECT id FROM users where username = ? ", $_POST["uname"]);
				$_SESSION["id"] = $info[0]["id"]; 
				render("radio_interface.php", ["title" => "radio", "js" => $js]);
			}
		}
	}
	else
	{
		// hashes the password
		$hash = $_POST["hash"];
		$hash = hash("md5",$hash); 	

		// Attempts to insert a new user
		$state = mysql::query("INSERT INTO users( name, username, hash, gender ) VALUES (? , ? , ? , ?)", $_POST["fullname"], $_POST["username"], $hash, $_POST["sex"]);
		// on failure, the username is probably taken
		if(!$state)
		{
			echo("This username name is already taken. please pick another one");
		}
		else
		{
			// on success logs in the user
			$info = mysql::query("SELECT id FROM users where username = ? ", $_POST["username"]);
			$_SESSION["id"] = $info[0]["id"]; 
			print("ok");
		}
	}
	
	
}
?>
