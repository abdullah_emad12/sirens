<?php
/*
* Displays the info of the song being currently played
*/
require('../includes/config.php');

$css = '<link href="/css/info.css" rel = "stylesheet" type = "text/css"/>';

$info = sound_info();
// offline connection
if($info == false)
{
	render('apology.php', ["title" => "Nothing to be viewed", "error" => "The radio seems to be offline at the moment. Please try again later"]);
}
else 
{
	$info['img'] = song_cover($info['img']);
	render('info_view.php', ["title" => "Media Info","info" => $info, "css" => $css]);
}

?>