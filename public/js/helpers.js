/*
**This includes all the functions that verifies the syntax of the registration and login info
**
*/
function strsub (str, sub)
{	
	var check = 0;
	for(var i = 0, n = str.length; i < n; i++)
	{
		for(var j = 0, n1 = sub.length; j < n1; j++)
		{
			if(str.charAt(i) == sub.charAt(j))
			{
				check++;	
			}
		}
		if(check == sub.length)
		{
			return true;
		}
		check = 0;
	}
}
function checkchar(str, pattern)
{
	for(var i = 0, n = pattern.length; i < n; i++)
	{
		for(var j = 0, n1 = str.length; j < n1; j++)
		{
			if(pattern.charAt(i) == str.charAt(j))
			{
				return true;			
			}
		}
	}
	return false;
}

function strcmp(str1, sub)
{	
	var temp = "";
	for(var i = 0, n = str1.length; i < n + 1; i++)
	{
		
		if(str1.charAt(i) == ' ' || i == n)
		{
			if(temp == sub)
			{
				return true;
			}
			temp = "";
		}
		else
		{
			temp = temp + str1.charAt(i);
		}
	}
	return false;
}

function username(str, data)
{
	
	for(var key of data.adjectives)
	{
		if(strcmp(str, key))
		{
			return false;
		}
	}
	if(checkchar(str,"!@#$%^&*()~:}{?><+|-=`/ "))
	{
		return false;
	}
	return true;
}
function fullname(str, data)
{
	for(var key of data.adjectives)
	{
		if(strcmp(str, key))
		{
			return false;
		}
	}
	if(checkchar(str,"!@#$%^&*()~:}{?><+|-=`/_"))
	{
		return false;
	}
			
	return true;
}

/*
* returns an arraybuffer created from the string str
* Code adapted from http://stackoverflow.com/questions/6965107/converting-between-strings-and-arraybuffers
*/
function create_arrbuf(str)
{
	// creates new instance of array of buffer
	var buf = new ArrayBuffer(str.length);
	var bufView = new Uint8Array(buf);
	// stores every equivalent character in the arraybuffer
	for (var i=0, strLen=str.length; i<strLen; i++) 
	{
    	bufView[i] =  str.charCodeAt(i);
  	}

  	// returns arraybuffer 
  	return buf;
}

