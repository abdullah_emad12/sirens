
// global variables
var socket; // the socket of the current connection 
var buffer; //All the buffers
var radio;
var buffSize = 0; //total size of the buffer. Updated whenever the radio stopps playing 

// global constants
const mime = 'audio/mpeg'; //will play MP3 frames 




//FLAGS
var play = false;




// runs whenever the document is ready
$(document).ready(function(){

	// when the control button is clicked (play/stop)
	radio =  document.querySelector('audio');
	$("#play").click(connect);
	$("#stop").click(disconnect);
});


/*
*connects to the server when the user requests a new 
*/
function connect()
{
	play = true;
	// creates new instances
	var mediaSource = new MediaSource();
	// adds the media source to the Audio element
	radio.src = URL.createObjectURL(mediaSource);	
	radio.preload = 'none';

	// waits 15 seconds and disconnects if now connection was alreadt established
	setTimeout(function(){
		if(radio.readyState != 4)
		{
			alert("Connection establishment took way too long! Check your internet connection and try again.");
			disconnect();
		}
	}, 10000);
	// when all the buffer ends	
		radio.addEventListener('waiting', function(){
			// clears the past connection
			buffSize = 0;
			changeState('load');
			this.pause();
			socket.disconnect();
			// tries to establish a new connection
			connect();	
	});

	// prints the current time ever 5 seconds
	printTime();

	// the source is opened 
	mediaSource.addEventListener('sourceopen',onSourceOpen);
}


/*
* Triggered when the readyState is changed to open
*/
function onSourceOpen()
{
	buffer = this.addSourceBuffer(mime);
	buffer.mode = 'sequence';

	// establishes a connection with the server on port 1200
	socket = new io(':1200');
	socket.on('connect_error', function(e){
		alert("Couldn't connect to the server! maybe the server is offline or you internet connection is down");
		disconnect();
	});

	// when the connection is down
	socket.on('disconnect', function(_){
		console.log('disconneted');
	});

	changeState('load');

	// asks for packets and plays them
	socket.on('transmission',onTransmittion);
}



/*
* Triggered when ever there is a new Transmission
*/
function onTransmittion(data)
{	
	// the server is offline
	if(typeof(data) == 'string')
	{
		if(data = 'off')
		{
			alert('Nothing is broadcasting now! please try again later');

		}
	}

	// adds the buffer to the bufferslist of the media source 
	var buf = data.buffer;
	if(buffSize == 4)
	{
		radio.play();
		changeState('play');
	}
	buffSize++;
	try{
		buffer.appendBuffer(buf);
	}catch(e)
	{
		console.log("Error on Appending buffer: " + e.message);
	}
	
}






/*
* Creates an event that changes the icon of the button
*/
function changeState(event)
{
	if(event == 'play')
	{
		$("#stop").css("visibility", "visible");
		$("#play").css("visibility", "hidden");
		$("#load").css("visibility", "hidden");
	}
	else if(event == 'stop')
	{	
		$("#play").css("visibility", "visible");
		$("#stop").css("visibility", "hidden");
		$("#load").css("visibility", "hidden");
	}
	else if(event == 'load')
	{
		$("#load").css("visibility", "visible");
		$("#stop").css("visibility", "hidden");
		$("#play").css("visibility", "hidden");
	}
}



/*
* Disconnects the socket from the server
*/

function disconnect()
{
	if(socket != undefined)
	{
		socket.disconnect();
		changeState('stop');
		buffSize = 0;
		play = false;
		radio.pause();	
	}
}

/*
* show error pop up
*/
function alert(message)
{
	console.log(message);
	var popup = document.getElementById("myPopup");
	popup.textContent = message;
   	popup.classList.toggle("show");
   	setTimeout(function(){
   		popup.classList.toggle("show");
   	}, 5000);
}

/*
* keeps printing the time every 15 seconds while playing
*/
function printTime()
{
	if(play)
	{
		console.log('Current Time: ' + radio.currentTime);
		setTimeout(printTime, 15000);
	}
}
/*
* Error Handling
*/
function failed(e) 
{
   // audio playback failed - show a message saying why
   // to get the source of the audio element use $(this).src
   switch (e.target.error.code) {
     case e.target.error.MEDIA_ERR_NETWORK:
       alert('A network error caused the audio download to fail.');
       break;
     case e.target.error.MEDIA_ERR_DECODE:
       alert('The audio playback was aborted due to a corruption problem or because the video used features your browser did not support.');
       break;
     case e.target.error.MEDIA_ERR_SRC_NOT_SUPPORTED:
       alert('The video audio not be loaded, either because the server or network failed or because the format is not supported.');
       break;
     default:
       alert('An unknown error occurred.');
       break;
   }
 }