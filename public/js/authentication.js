/**
*CS50 final project
*Abdullah Emad
**/
$(document).ready(function() {
	login();
	register();
	change_password();
});

function register(){
	$("#register_form").submit(function(){

			event.preventDefault();
			$("#error").html("");
			$.getJSON("/js/forbid.json", function(data){		
			var fname = $("#register_form input[name = name]").val();
			var uname = $("#register_form input[name = uname]").val();
			var password = $("#register_form input[name = password]").val();
			var password2 = $("#register_form input[name = password2]").val();
			var gender = $("#register_form input[name = gender]").val();
			if(!fname || !uname || !password || !password2 || !$("#register_form input[name = gender]").is(':checked'))
			{
				$("#error").html("You are missing fields. Please fill all the info and resubmit");
			}
			else if(!fullname(fname, data))
			{
				$("#error").html("your name shouldn't contain any funky characters or numbers e.g: *&$(@)$123 and make sure it's a name of a person");
			}
			else if(!username(uname, data))
			{
				$("#error").html("your name shouldn't contain any funky characters other than _ and make sure it's a name of a person");
			}
			else if(strsub(password,uname))
			{
				$("#error").html("Your password shouldn't include your username");
			}
			else if(password.length < 5)
			{
				$("#error").html("Password is too short. It must be five characters at least");
			}
			else if(password != password2)
			{
				$("#error").html("The two passwords does not match");
			}
			else
			{
				info ={
					fullname: fname,
					username: uname,
					hash: password,
					sex: gender
					};
				$.post("register.php", info, function(data){
					if(data == "ok")
					{
						window.location.replace("radio.php");
					}
					else
					{
						$("#error").html(data);				
					}
				});
			}
		});
		

	});
	
}

function login(){

	$("#login_form").submit(function(){

		event.preventDefault();
		$("#error").html("");	
		if($("#login_form input[name=name]").val() == "" || $("#login_form input[name=name]").val() == " ")
		{
			$("#error").html("You have to provide a username and a password");
		}
		else if($("#login_form input[name=password]").val() == "" || $("#login_form input[name=password]").val() == " ")
		{
			$("#error").html("You have to provide a username and a password");
		}	
		else
		{		
			console.log('test');
			info = {
				uname: $("#login_form input[name=name]").val(),
				password: $("#login_form input[name=password]").val()
			};









			console.log(info);
			$.post("login.php", info, function(data){
				if(data != "ok")
				{
					$("#error").html(data);
				}
				else
				{
					window.location.replace("radio.php");
				}
			
			});
		}
	});
}

function change_password()
{
	$("#changepass_form").submit(function(){
		event.preventDefault();
			$("#error").html("");
			if($("#changepass_form input[name=password]").val() == "" || $("#changepass_form input[name=password1]").val() == " "
				|| $("#changepass_form input[name=password2]").val() == " ")
			{
				$("#error").html("You have to fill all the fiields");
			}
			else if($("#changepass_form input[name=password]").val() == $("#changepass_form input[name=password1]").val())
			{
				$("#error").html("Please provide a new password");
			}
			else if($("#changepass_form input[name=password1]").val() != $("#changepass_form input[name=password2]").val())
			{
				$("#error").html("The two passwords does not match !");
			}
			else if($("#changepass_form input[name=password1]").val().length <= 4)
			{
				$("#error").html("Your password is too short please pick a more secured one");
			}
			else
			{
				var info = 
				{
					Oldpassword: $("#changepass_form input[name=password]").val(),
					Newpassword: $("#changepass_form input[name=password1]").val()
				};
				$.post("account.php", info, function(data){
					if(data != "ok")
					{
						console.log(data);
						$("#error").html(data);
					}
					else
					{
						window.location.replace("radio.php");
					}
				
				});
			}
		});
}
