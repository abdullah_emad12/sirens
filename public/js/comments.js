/*
* allow the user to post and delete comments without refreshing the page
*/

$(document).ready(function() {

	$(document).click(manageComment);
	$("#comment_field").keyup(showSubmit);
	$(".add_comment").submit(onSubmission);
	 
});


function manageComment(e)
{
	
	if(e.target && e.target.id == "delete")
	{	
		event.preventDefault();
		if(confirm('Are you sure you want to delete this comment?') == true)
		{
		 	var link = $(e.target).attr('href');
		 	var ind = link.indexOf("=") + 1;
		 	var id = link.substring(ind);
		 	var temp = $("#" + id);
		 	$.get(link, function(_){
		 		temp.remove();
		 	});
		}
	}



}
function onSubmission()
{
	event.preventDefault();
	var comment = $("#comment_field").val();
	var link = 'add_comment.php?comment=' + comment;
	$.get(link, insertComment);
}

/*
* Inserts a new comment to the page
*/
function insertComment(data)
{	
	//checks for errors
	if(typeof(data) != 'string')
	{
		return;
	}
	else if(data == 'error')
	{
		return;
	}

	// index of the fullname
	var ind = data.indexOf("/") + 1;
	var fullname  = data.substring(ind);
	var id  = data.substring(0, ind - 1);
	var time = new Date() + "";
	var comment = $("#comment_field").val();
	
	indtime =time.indexOf("GMT");
	time = time.substring(0, indtime);

	// creates the HTML element
	var deleteL = '<a href = "delete.php?id=' + id + '" id ="delete">Delete</a>';
	var html = '<div class = "row" id="' + id+ '">';
	html = html + '<div class = "col-md-1 col-xs-2"><img src = "/img/interface/default_user.png"/> </div>';
	html  = html + '<div class = "col-md-3 cols-xs-6">';
	html = html + '<p id = "name">' + fullname + '</p>';
	html = html + '<p id = "text">' +  comment + '</p>';
	html = html + '<p id = "date">' + time +  '  '  +  deleteL +'</p>'
	html = html + '</div></div>';
	$('.comments').prepend(html);
	$("#comment_field").val("");
	$("#submit").css("visibility" , "hidden");

}
/*
* Shows the submission button
*/
function showSubmit()
{

	if($(this).val() == "")
	{
		$("#submit").css("visibility" , "hidden");

	}
	else
	{
		$("#submit").css("visibility" , "visible");
	}


}

