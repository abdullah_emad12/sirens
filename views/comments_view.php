
<!--Comments section-->
<div class = "comments_section">

	<div class = "comments-container">
		<p id = "head">Share your thoughts here!</p>
		<div class = comments>
		<?php
			$n = sizeof($comments);
			for($i = 0; $i < $n && $i < 10; $i++)
			{
					$delete = "";
					$id = $comments[$i]['id'];
					if($comments[$i]['user_id'] == $_SESSION['id'])
					{ 
						$delete = "<a href = \"delete.php?id=$id\" id = \"delete\">Delete</a>";
					}
					echo("<div class = \"row\" id=\"$id\">");
					// profile picture
					echo('<div class = "col-md-1 col-xs-1"><img src = "/img/interface/default_user.png"/> </div>');
					// information		
					echo('<div class = "col-md-3 col-xs-6">');
					echo('<p id = "name">' . $comments[$i]['username'] . '</p>');
					echo('<p id = "text">' . $comments[$i]['comment'] . '</p>');
					echo('<p id = "date">' . $comments[$i]['created_at'] . '   '. $delete .'</p> ');
					echo('</div>');

		
					echo('</div>');

			}
		?>	
		</div>
	</div>	
	<div class = "add_comment">
		<form method="get" action = "add_comment.php" class = "form-horizontal">
			<div class = "form-group">
				<input id = "comment_field" class = "form-control col-md-12 col-xs-12" type = "text"  placeholder="Add a public comment" name = "comments" autocomplete="off">
			</div>
			 <div class="form-group"> 
		   		 <div class="col-md-offset-11 col-md-1 col-xs-offset-10 col-xs-2">
		      		<button type="submit" class="btn btn-default" id = "submit" >Submit</button>
		   		</div>
		  </div>
	  </form>
	</div>
</div>
