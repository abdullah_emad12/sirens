<!DOCTYPE html>
<fieldset class = "register">
	<div id = "title">Don't have an account yet ? <div style = "color:#afaba3; display:inline-block">Register</div></div>
		<form method = "post" action = "register.php" id = "register_form">
			<div class = "form-group">
				<input class = "form-control" type = "text" placeholder = "Fullname" autocomplete = "on" name = "name">			
			</div>
			<div class = "form-group">
				<input class = "form-control" type = "text" placeholder = "Username" autocomplete = "on" name = "uname">			
			</div>
			<div class = "form-group">
				<input class = "form-control" type = "password" placeholder = "Password" name = "password">			
			</div>
			<div class = "form-group">
				<input class = "form-control" type = "password" placeholder = "Confirm Password" name = "password2">			
			</div>
			<div class = "form-group">
				<div id = "radio"><input type = "radio" value = "male" name = "gender"> Male</div>
				<div id = "radio"><input type = "radio" value = "female" name = "gender"> Female</div>		
			</div>
			<div id = "error"></div>
			<div class = "form-group">
				<button type = "submit" class = "btn btn-default">
					<span aria-hidden="true" class="glyphicon glyphicon-register"></span>
					Register
				</button>
			</div>
		</form>
		<p>Already Registered ? <a href = "login.php">Login</a></p>
</fildset>
