<!--Introduction section -->
<div class = "row" id = "intro">
	<div class = "col-md-6  col-xs-6">
			<div class = "row">
					<div class = "col-md-12 col-xs-12"><h2><b>Welcome To Sirens Radio</b></h2></div>
			</div>
				<div class = "row">
					<div class = "col-md-12 col-xs-12">
						<p>
							Real-time music and live audio transmission and broadcasts. Sirens was intentionally made for the CS50 final project. It's completely Free - Feel comfortable to explore and register for an account and let your friends know about. Abdullah Emad 
						</p>
					</div>
				</div>
	</div>

	<div class = "col-md-offset-2 col-md-4 col-xs-offset-2 col-xs-4">
		<filedset class = "login">
			<img  id = "logo_art" src = "/img/logo_art.png">
			<div id = "title">Already Registered? <div style = "color:#afaba3; display:inline-block">Login</div></div>
			<form method = "post" action = "login.php" id = "login_form">
				<div class = form-group>
					<input class = "form-control" type="text" autocomplete = "on" placeholder = "Username" name = "name">
				</div>
				<div class = "form-group">
					<input class = "form-control" type="password" placeholder="password" name = "password">
				</div>
				<div id = "error"></div>
				<div class = "form-group">
					<button type = "submit" class = "btn btn-default">
						<span aria-hidden="true" class="glyphicon glyphicon-log-in"></span>
						Log in
					</button>
				</div>
			</form>
		</filedset>
		<p>Don't have an account ?<a href = "register.php"> Sign up</a></p>

	</div>
</div>
