
<?php extract($info); 
date_default_timezone_set('Africa/Cairo');
?>

<div class = "item">
	<div id = "icon" class = "row">
		<div class = "col-md-3 col-xs-3">
			<img src=<?= '"' . $img . '"' ?>alt = <?= '"' . $name . '"' ?>>
		</div>
		<div class = "col-md-8 col-xs-8">
			<div class = "row" id = "title">
				<p><?=$name?></p>
			</div>
			<div class = "row" id = "artist">
				<p>Featuring: <b> <?=$artist?></b></p>
			</div>
			<div class = "row" id = "time">
				<p>Time Now: <?= date("h:i:sa") . " " . date("d/m/Y")?> </p>
			</div>
		</div>
	</div>

</div>


