<!DOCTYPE html>

<html>
	<head>
		<!--General style  -->
		<link href = "/css/style.css" rel= "stylesheet"/>
		<?php if(isset($css)) echo($css)?>	

		<link href = "/css/radio_interface.css" rel = "stylesheet"/>

		<!-- http://getbootstrap.com/ -->
		<link href="/css/bootstrap.min.css" rel="stylesheet"/>

		<!-- http://jquery.org-->
		<script src="/js/API/jquery-3.1.1.min.js"></script>
		
		<!-- Scripts -->		

		<?php if(isset($js)) echo($js)?>					
		
		<!-- Website icon -->
		<link rel="icon" type="image/ico"  href="/img/favicon.ico">
		<title>
			<?php
				if(isset($title))
				{
					echo($title);
				}
				else
				{
					echo("CS50 Radio");
				}
			?>
		</title>
	</head>
	<!-- background image source: https://www.pexels.com/photo/woman-wearing-white-long-sleeve-sitting-beside-window-during-daytime-24385/ -->	
	<body>
		<nav class="navbar navbar-inverse">
		  <div class="container-fluid">
		    <div class="navbar-header">
		    <a href="/" class="navbar-left"><img id = "logo" src="img/logo.png" alt = "Sirens"></a>
		    </div>
		    <ul class="nav navbar-nav">
		    </ul>
		    <ul class="nav navbar-nav navbar-right">
		      <li><a href= <?=$account?>><span class="glyphicon glyphicon-user"></span> <?=$accountStr?></a></li>
		      <li><a href= <?=$log?>><span class="glyphicon glyphicon-log-in"></span> <?=$logStr?></a></li>
		    </ul>
		  </div>
		</nav>
		<div class = "container">