<!DOCTYPE html>
<fieldset class = "register">
	<div id = "title"><?=$fullname?> <div style = "color:#afaba3; display:inline-block"><?=$username?></div></div>
		<form method = "post" action = "account.php" id = "changepass_form">
			<div class = "form-group">
				<input class = "form-control" type = "password" placeholder = "Old Password" name = "password">			
			</div>
			<div class = "form-group">
				<input class = "form-control" type = "password" placeholder = "New Password" name = "password1">			
			</div>
			<div class = "form-group">
				<input class = "form-control" type = "password" placeholder = "Confirm Password" name = "password2">			
			</div>
			<div id = "error"></div>
			<div class = "form-group">
				<button type = "submit" class = "btn btn-default">
					<span aria-hidden="true" class="glyphicon glyphicon-register"></span>
					Change Password
				</button>
			</div>
		</form>
		<p>Do you want to deactivate you account :( ?
		<b><a href="deactivate.php" id = "deactivate">Deactivate</a></b>
		</p>
</fildset>
