<style>
	body
	{	


	  display: block;
	  position: relative;	

	}

	body::after
	{
		content: "";
		opacity: 0.5;
		top: 0;
		left: 0;
		bottom: 0;
		right: 0;
		position: fixed;
		height: 100%;
		width:100%;
		z-index: -1;   
		 background: url("img/about/Siren-myth.png") no-repeat center center fixed; 
	   -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
        background-color: white;
	}
	p
	{
		opacity: 1;
		font-size: 16px;
		font-weight: bold;
		color:black;
		z-index: 0;
	}
	.about
	{
		 background-color: transparent;
		 margin-bottom: 10%;
	}
	
	h1
	{
		color: black;
	}
</style>

<div class = "about">
	<div class="row">
		<div class = "col-md-offset-3 col-md-8 col-xs-offset-2 col-xs-8">
			<a  href = "img/about/logo.jpg"><img src ="img/about/headline.png"/></a>
		</div>
	</div>
	<div class = "row">
		<div class = "col-md-offset-1 col-md-10 col-xs-offset-1 col-xs-11">
			<h1><b>Sirens Myth</b></h1>
			<p>
				In Greek mythology, Sirens were sea creatures akins to mermaids. They were very beautiful with a haunting singing voice. A Siren would hypnotize sailors with the sweetness of her song, and eventually causes them to lose control over their ship.
			</p>
			<h1><b>Contents of the website</b></h1>
			<p>
				This is an online radio where you can stream live, real-time audio or listen to live voice transmissions.In order to be able to view and turn the radio on you must first rigester and sign in with your account. No Email or sms verfication is required. you can leave comments in the comments section (maybe you like something, dislike something, want to listen to a specific song). You can also click on the radio interface logo and view the music info page which has information about the song being currently played: The title of the song, the artist and the cover picture. You can change you password or even deactivate your account from the account page. This website was intended to be my CS50 final project and was not created for official use.Feel free to try out this website and even register with as many accounts as you want.... and 
				let me know what you think.
			</p>
			<h1><b>Compatibility</b></h1>
			<p>
				The website is not 100% stable. This website requires javascript and a modern web browser; however, it's compatible  with smart phones and tablets that has google chrome or similar modern web browser installed.
			</p>
			<h1><b>Acknowledgement</b></h1>
			<p>
				After a huge amount of time, failures and effort I was finally able to finish my CS50 final project. Nevertheless, I did learn and accomplish things I never thought I was capable of before starting this course. I want to thank Prof. David, the CS50 staff and my family and friends for all the things I learned until now and for all the things I will learn later on.   
			</p>
			<h1><b>Refrences</b></h1>
			<p>
				<ul>
					<li>
						Saleh, A. E. (2017, May 31). Abdullahemad12/sirens. Retrieved June 14, 2017, from <a href = "https://gitlab.com/abdullah_emad12/sirens">GitLab</a>
					</li>
					<li>
						APA MLA Chicago The Island of the Sirens. (n.d.). Retrieved June 14, 2017, from <a href = "https://www.tripline.net/trip/Odysseus%27s_travel_to_Ithaca.-607116077066100499FC9C91D4239303">tripline</a>
					</li>
					<li>
						credit for the Audio controllers, i.e. "Play" and "Stop" buttons, goes to <a href="https://icons8.com">Icon pack by Icons8</a>
					</li>
					<li>
						Credits for the home background photo goes to <a href="https://unsplash.com/@killerfvith">Alex wong</a>
					</li>
				</ul>
			</p>
		</div>

	</div>
</div>
