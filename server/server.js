'use strict';
// imports 
// imports initialization
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var sleep = require('sleep');
var helpers = require('./helpers');
var fs = require('fs');
/*
*  	Class that opens a FIFO tunnel with broadcast.c and buffers MP3 frames to be emitted to the client
*/
'use strict'

class Tunnel
{
	constructor()
	{
		var strict = require('use-strict');
		this.fifo = require('fifo-js');
		this.buffer;
	}

	/*
	* Takes the buffer from broadcast and transmits it to the client
	*/
	transmit(socket)
	{
		var fifo = new this.fifo("/tmp/radio");
		fifo.setReader(function(data)
		{	
			if(helpers.State() == -1)
			{
				console.log("Error: Broadcast is not running\nExiting...");
				process.exit();
			}
			var contents = fs.readFileSync('/home/abdullah/Music/buffer');
			var buf = new Buffer(contents);
			socket.emit('transmission', {buffer: buf});
		});
		socket.on('disconnect', function(socket){
			console.log("A user just disconnected")
			fifo.close();
		});
	}
}

	

	/*
	*	is triggered when a user emit "feed"
	*/ 
	function onConnection(socket)
	{
		if(helpers.State == -1)
		{
			socket.emit('off');
		}
		helpers.notifyConnection(socket);
		// creates a new request with buffer
		var tunnel = new Tunnel();
		tunnel.transmit(socket);

	}

	// Main

	
	app.get('/', function(req, res){
		res.sendFile(__dirname + '/index.html');
	});



	io.on('connection', onConnection);




	// starts listening to requests 
	http.listen(1200, function(){
	  console.log('listening on *:1200');
	});

	

