var users = 0;
/*
* Prints the IP Adress of the user 
*/
function notifyConnection(socket)
{
	if(socket == undefined || socket.handshake == undefined)
	{
		return;
	}
	var ip = socket.handshake.address;
	var ind = 1 +  ip.indexOf(':', 2);
	console.log("Connection Established With: " + ip.substring(ind));
}

/*
* Calculate the size of a give file
*/

function fileSize(location)
{
	const fs = require("fs"); //Load the filesystem module
	const stats = fs.statSync(location);
	return stats.size;
}


/*
* Reads and return the state of broadcast
*/
function State()
{
	var fs = require('fs');
	var fd = fs.readFileSync('../broadcast/.state');
	return fd.readInt8();
}


module.exports = {notifyConnection,fileSize,State};