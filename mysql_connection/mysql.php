<?php

/**
*This class mysql was done by Abdullah Emad based on the CS50 class 
*some changes in the structure were made
*
**/

class mysql
{
	private static $config;
	
	// constructor for the mysql class
	public static function init($path)
	{
		// checks if the config file exists
		if(!file_exists($path))
		{
			trigger_error("Couldn't load MySQl Configuration", E_USER_ERROR);
		}

		// reads and stores the config file as a string
		$content = file_get_contents($path);
		if($content == false)
		{
			trigger_error("Couldn't read the configuration", E_USER_ERROR);
		}
		//converts this json string into an associative array
		$config = json_decode($content, true);

		if(is_null($config))
		{
			trigger_error("Syntax error in the configuration file", E_USER_ERROR);
		}

		// sets the instance config to authentication info
		self::$config = $config;
	}
	public static function query(/*can take many arguments*/)
	{
		// checks if no arguments are provided
		if(is_null(func_get_arg(0)))
		{
			trigger_error("query must take at least one argument");
		}
		// checks all the key values		
		foreach(["host", "name", "username", "password"] as $key)
		{
			if(!isset(self::$config["database"][$key]))
			{
				trigger_error("Missing a key value in the configuration file. '{$key}'", E_USER_ERROR);
			}
		}
		
		//stores the query and the parameters
		$query = "";
		$query = func_get_arg(0);
		$inputs = array_slice(func_get_args(), 1);
		extract(self::$config["database"]);
		
		static $connection;
		
		//attempts to establish a connection		
		try
		{
			$connection = new PDO("mysql:host=" . $host . ";dbname=" . $name
			, $username
			,$password);
		}
		catch(Exception $e)
		{
			trigger_error($e->getMessage(), E_USER_ERROR);
		}
		
		// checks for the correct pattern
		self::pattern($query, count($inputs));
		
		// generates the final query string
		$sql = self::query_generate($query, $inputs);
		
		$statement = $connection->query($sql);

		// checks for errors
		if($statement == false && $connection->errorCode() != 23000)
		{
			trigger_error($connection->errorCode() , E_USER_ERROR);
		}
		if($connection->errorCode() == 23000)
		{
			return false;
		}

		// checks if column or information was return. USAGE : SELECT
		if($statement->columnCount() > 0)
		{
			return $statement->fetchAll(PDO::FETCH_ASSOC);
		}

		//USAGE: INSERT DELETE UPDATE
		else
		{
			return $statement->rowCount();
		}
		
		
	
	}
	//checks if the number of parameters matches the number of the placeholders	
	private static function pattern($query, $n)
	{
		$pattern = "
                /(?:
                '[^'\\\\]*(?:(?:\\\\.|'')[^'\\\\]*)*'
                | \"[^\"\\\\]*(?:(?:\\\\.|\"\")[^\"\\\\]*)*\"
                | `[^`\\\\]*(?:(?:\\\\.|``)[^`\\\\]*)*`
                )(*SKIP)(*F)| \?
                /x
            ";
		preg_match_all($pattern, $query, $matches);
		if(count($matches[0]) > $n)
		{
			trigger_error("Too few parameters", E_USER_ERROR);	
		}
		else if(count($matches[0]) < $n)
		{
			trigger_error("Too many parameters", E_USER_ERROR);	
		}
		return 0;
	}
	
	private static function query_generate($query, $inputs)
	{
		$sql = ""; 
		$j = 0;
		// iterates over the input query
		for($i = 0, $n = strlen($query); $i < $n; $i++)
		{
			// replaces the place holders with the proper values
			if($query[$i] == "?")
			{
				$sql = $sql ."'" . $inputs[$j] . "'";
				$j++;
			}
			else
			{
				$sql = $sql . $query[$i];
			}
		}
		
		return $sql;
	}
}

?>
