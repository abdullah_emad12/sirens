<?php 

ini_set("display_errors", 'true');
error_reporting(E_ALL);

require("helpers.php");
require("../mysql_connection/mysql.php");
$path = __DIR__ . "/../.config.json";

mysql::init($path);


session_start();

// in case the user is logged in we don't want him to be able to access every page
if(!isset($_SESSION["id"]))
{
	if(!in_array($_SERVER["PHP_SELF"], ["/login.php", "/register.php", "/about.php"]))
	{
	
			header("Location: login.php");
	}
}
// pages that the user can't access when he is logged in
else
{
	 if(in_array($_SERVER["PHP_SELF"], ["/login.php", "/register.php", "/index.php"]))
	{
		
			header("Location: radio.php");
	}
} 
?>
