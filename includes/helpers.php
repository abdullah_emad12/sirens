<?php 
require_once("config.php");

/**
** Renders the requested page
**/
function render($view , $values = [])
{
	// setting different header parameters
	if(!isset($_SESSION["id"]))
	{
		$account = '"register.php"';
		$accountStr = 'Sign Up';
		$log = '"login.php"';
		$logStr = 'Login';
	}
	else
	{
		$account = '"account.php"';
		$accountStr = 'Account';
		$log = '"logout.php"';
		$logStr = 'Logout';
	}
	extract($values);
	if (file_exists("../views/{$view}"))
    {	
		require("../views/header.php");
		require("../views/" . $view);
		require("../views/footer.php");
		exit();
	}
	else
	{
		trigger_error("{$view} doesn't exist", ERROR_USER_E);
	}
}

/*
* opens a FIFO connection with broadcast and retrieves the info of the current sound being played
*/
function sound_info()
{
	$stat;
	$command = "ps -C broadcast -o pid="; 
	$output;
	// makes sure broadcast is running
	exec($command, $output, $stat);
	if($stat == 1)
	{
		return false;
	}
	// attempts to open the FIFO
	$pipe = fopen("/tmp/radio", "r");
	
	// check if the connection fails
	if($pipe == false)
	{
		return false;
	}
	
	// reads the size of the information being send
	$s = fread($pipe, 4);
	if($s == false)
	{
		return false;
	}

	// reads the header
	$size = ord($s);
	$header = fread($pipe, $size);
	
	// the keys of the new array
	$keys = [0 => "name", 1 => "artist" , 2 => "location"];  
	$info = [];
	
	// the start at which the string will be read
	$i = 0 ;

	// extracts the info from the string "n of key" times
	foreach($keys as $key)
	{
		$n = strpos($header, "\n", $i);
		
		$value = substr($header,$i ,$n - $i);
		$info[$key] =  $value;
		$i = $n + 1;
	}
	

	// checks for Images JPEG BMP GIF PNG 
	$pos = strpos($info["location"], ".mp3", 0);
	$img = substr($info["location"],0 , $pos);
	// checks the file existance with all the popular image formats
	$jpg = $img . ".jpg";
	$bmp = $img . ".bmp";
	$gif = $img . ".gif";
	$png = $img . ".png";

	if(file_exists($jpg))
	{
		$info["img"] = $jpg;
	}
	
	else if(file_exists($bmp))
	{
		$info["img"] = $bmp;
	}

	else if(file_exists($gif))
	{
		$info["img"] = $gif;
	}

	else if(file_exists($png))
	{
		$info["img"] = $png;
	}
	else
	{
		$info["img"] = "none";
	}
	// makes sure the array consists of at least 3 items
	if(count($info) < 2)
	{
		return false;
	}
	// success
	return $info;
}
/**
** Creates a cover image for the song if it does not exist
**/
function song_cover($path)
{
	// checks if the file does not exist
	if(!file_exists($path))
	{
		return "img/media_ico/default.jpg";
	}
	
	// creates a new path for the new image

	$n = strrpos($path, "/", 0);
	$temp_path = substr($path ,$n ,strlen($path) - $n);
	$out = "img/media_ico" . $temp_path;
	// checks if we are not recreating the file
	if(file_exists($out))
	{
		return $out;
	}
	

	// opens the source image for reading
	$img = fopen($path, "r");

	// opens the destination for writing
	$new_img = fopen($out, "w");
	if($img == false || $new_img == false)
	{
		return "img/media_ico/default.jpg";
	}
	// reads the source image
	$size = filesize($path);
	$data_out = fread($img, $size);

	// writes to the destination
	fwrite($new_img, $data_out, $size);
	fclose($img);
	fclose($new_img);
	
	// changes back the permission of the file
	

	return $out;
}


/*
* Checks to see if broadcast is running
*/

function check_broadcast()
{
	$path = '../broadcast/.state';
	// checks if the file exists first to avoid server errors
	if(!file_exists($path))
	{
		return false;
	}
	$file = fopen($path, "r");
	$size = filesize($path);

	if($size <= 0)
	{
		return false;
	}
	$state = fread($file, $size);
	fclose($file);


	$state = ord($state);
	// checks state
	if($state == 255)
	{
		return false;
	}
	else
	{
		return true;
	}

}

/*
* Gets all the comments in the data base and returns as a 2D array
*/
function fetch_comments()
{
	$all_comments = mysql::query("SELECT * FROM `comments` ORDER BY `created_at` DESC");

	// no comments were found
	if(sizeof($all_comments) == 0)
	{
		return [];
	}

	// extracts the information --- only the latest 10 comments or all the comments in case of less than 10 comments
	for($i = 0; $i < 10 && $i < sizeof($all_comments); $i++)
	{
		// add thes username to the array to be displayed
		$username = mysql::query("SELECT * FROM `users` WHERE `id` = ?", $all_comments[$i]['user_id']);
		$all_comments[$i]['username'] = $username[0]['name'];
	}
	return $all_comments;

}
?>
