/*
* Saleh, A. E. (2017, May & june). mpg (Version 1) [Computer software].
*
* Made for reading and interpreting MP3 Files Mainly
*
*/


// for the time.h
#define _POSIX_C_SOURCE 199309L


#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <lame/lame.h>
#include <time.h>
//headers
#include "headers/mpg.h"
#include "headers/capture.h"



// variables 
int frame_count = 0;
int forward_count = 0;

/**
** The Bitrate of every Combination of layers and version (in kbps)
** rows: 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 
** Columns: V1,L1   V1,L2   V1,L3   V2,L1   V2,L2&L3
**/
unsigned int bitrate[16][5]=
{
	{FREE, FREE, FREE, FREE, FREE},
	{32000, 32000, 32000, 32000, 8000},
	{64000, 48000, 40000, 48000, 16000},
	{96000, 56000, 48000, 56000, 24000},
	{128000, 64000, 56000, 64000, 32000},
	{160000, 80000, 64000, 80000, 40000},
	{192000, 96000, 80000, 96000, 48000},
	{224000, 112000, 96000, 112000, 56000},
	{256000, 128000, 112000, 128000, 64000},
	{288000, 160000, 128000, 144000, 80000},
	{320000, 192000, 160000, 160000, 96000},
	{352000, 224000, 192000, 176000, 112000},
	{384000, 256000, 224000, 192000, 128000},
	{416000, 320000, 256000, 224000, 144000},
	{448000, 384000, 320000, 256000, 160000},
	{BAD, BAD, BAD, BAD}
};
/**
** The sample rate according to the version (Hz)
** Columns:	MPEG2.5 MPEG1 MPEG2  
** Rows: 0 1 2 Reserved
**/
unsigned int sample_rate[4][3] = 
{
	{11025, 44100, 22050},
	{12000, 48000, 24000},
	{8000, 32000, 16000},
	{RESERVED, RESERVED, RESERVED}
};

/**
** Samples per frame. Colums: layerI LayerII&LayerIII
**/
unsigned int spl[3] = {384, 1152, 1152};

/**
** Array with The equivalent number of layers
**/

unsigned int layer[4] = {RESERVED, 3, 2, 1};

/**
** Array that reorders the version
**/

unsigned int version[4] = {3, 0, 2, 1};

/**
** Array that specifies the sample per frame
** use - [layer][version]
**/

int samples_frame[4][4] = 
{
	{RESERVED, RESERVED, RESERVED , RESERVED},
	{RESERVED, 384, 384, RESERVED},
	{RESERVED, 1152, 1152, RESERVED},
	{RESERVED, 1152, 576, RESERVED}

};
/**
** Decodes and encodes the Synchsafe bits
** synchsafe and unsynchsafe Functions were adapted from http://stackoverflow.com/questions/5223025/why-are-there-synchsafe-integer
**/

LONG synchsafeDecode(LONG decode)
{
  	// converts from big endian to little indian if necessary
	LONG l_endian = ntohl(decode);
	//divides the sum on four variables and aligns them
	int Sa = l_endian & 0x7f000000;
	int Sb = l_endian & 0x7f0000;
	int Sc = l_endian & 0x7f00;
	int Sd = l_endian & 0x7f;
	
	// removes every zero in the seventh bit abcd and stores the result in sum

	return Sa >> 3 | Sb >> 2 | Sc >> 1 | Sd; 
}

/**
** looks for the Frame sync in case the MP3 file was not uniformally stuctured
**/
int Framesync_lookup(FILE* media)
{
	LONG sync;
	// keeps reading until the framesync is found or EOF is reached
	while(fread(&sync, sizeof(LONG), 1, media) > 0)
	{	
		sync = ntohl(sync);
		sync = (sync >> 21) & 0x7FF;
		// framesync is found
		if(sync == 0x7FF)
		{
			fseek(media, -sizeof(LONG), SEEK_CUR);
			return 0;
		}
		else
		{
			// moves backward one byte to make sure all the file has been scanned 
			fseek(media, 1 - sizeof(LONG), SEEK_CUR);
		}
	}
	return ERROR;
}

/**
** Makes sure the file is supported and returns the correct size
** returns ERROR on errors or the size of the ID3V2 Tag
** if the ID3V2 tag was not found,INVALID is returned and the media will be set at the first frame 
**/
int mp3Header(FILE* media)
{
	ID3TAG tag;
	// Reads the ID3V2 Tag 
	fread(&tag, sizeof(ID3TAG), 1, media);
	LONG _id[3];
	
	//stores the id tag in a 32-bit variable
	for(int i = 0; i < 3; i++)
	{
		// prepares the three variables to align them together
		_id[i] = tag.id[i];
		_id[i] = _id[i] << 8*(2-i);
	}
	
	// align the three variables together
	LONG id = _id[0] | _id[1] | _id[2];
	
	// checks for valid mp3 id
	if(id != 0x494433)
	{
		fseek(media ,0 ,SEEK_SET);
		// skips to the first frame if it exists
		if(Framesync_lookup(media) == ERROR)
		{
			return ERROR;		
		}
		else
		{
			return INVALID;
		}
	}
	//decodes the sync safe bits
	int size = synchsafeDecode(tag.size);		
	// checks for the existence of footer
	BYTE footer = tag.flags;

	// removes all the other 'ones' except that indicating the presence of the footer 
	footer = footer & 0x10;
	footer = footer >> 4;
	
	// gives 10 bytes if the footer is present
	footer = footer * 10;
	fseek(media, size + footer , SEEK_CUR);
	return size + footer ;
}

/**
**  Looks for all the information related to the song (if ID3V1 exist)
**	writes the name of the song and artist to the passed char* and returns 0 on success
**/
int songInfo(FILE* media ,char* song, char* artist)
{
	// skips over to the last 128-bytes
	int cur_pos = ftell(media);
	fseek(media, -sizeof(ID3V1), SEEK_END);

	//reads the song's info	
	ID3V1 tag;
	fread(&tag, sizeof(ID3V1), 1, media);

	LONG _id[3];
	//stores the id tag in a 32-bit variable
	for(int i = 0; i < 3; i++)
	{
		// prepares the three variables to align them together
		_id[i] = tag.tag[i];
		_id[i] = _id[i] << 8*(2-i);
	}
	// align the three variables together
	LONG id = _id[0] | _id[1] | _id[2];
	
	// checks if this was an ID3V1 tag	
	if(id != 0x544147)
	{
		fseek(media, cur_pos, SEEK_SET);
		return -1;
	}
 	int empty = 0;
	for(int i = 0; i < 30; i++)
	{	
		// checks if the 30 characters of the array are empty
		if(tag.title[i] == 0)
		{
			empty++;
		}
		song[i] = tag.title[i];
		artist[i] = tag.artist[i];
	}
	fseek(media, cur_pos, SEEK_SET);
	
	// returns failure if Empty
	if(empty == 30)
	{
		return ERROR;
	}
	return 0;
	
}

/**
** writes time and size related info to the given variables 
** media is the MP3 file that will be read
** ms is the bits/second rate at which the MP3 will be played
**/

int frameInfo(FILE* media, FRAMEHEADER* hdr)
{
	if(hdr == NULL)
	{
		return -1;
	}
	// reads the header of the frame 
	LONG frame; 
	if(fread(&frame, sizeof(LONG), 1, media) <= 0)
	{
		return EOF;
	}
	// converts the bits from big endian to little endian	
	frame = ntohl(frame);
	
	// writes all the info to the FRAMEHEADER struct
	hdr->framesync = frame >> 21 & 0x7ff;
	hdr->version = (frame >> 19) & 0x3;
	hdr->layer = (frame >> 17) & 0x3;
	hdr->protection = (frame >> 16) & 0x1;
	hdr->bitrate_index = (frame >> 12) & 0xf;
	hdr->sample_rate = (frame >> 10) & 0x3;
	hdr->padding = (frame >> 9) & 0x1;
	hdr->private_bit = (frame >> 8) & 0x1;
	hdr->channels = (frame >> 6) & 0x3;
	hdr->extension = (frame >> 4) & 0x3;
	hdr->cpy_right = (frame >> 3) & 0x1;
	hdr->original = (frame >> 2) & 0x1;
	hdr->emphasis = frame & 0x2;
	
	if(hdr->framesync != 0x7FF)
	{
		if(Framesync_lookup(media) == EOF)
		{
			return EOF;
		}
		frameInfo(media, hdr);
	}
	return 1;
	
}

/**
**
** Returns the Bitrate in kbps of the MP3 according to the layer and version
** Returns a negative number on failure
**
**/
int  bitrateVal(FRAMEHEADER hdr)
{
	int ver = version[hdr.version];
	int lyr = layer[hdr.layer];
	int b_rate = 0;
	// stores the frequency according to the version and layer
	switch(lyr)
	{
		// case layer 1
		case 1:
		{
			if(ver == 1)
			{
				b_rate = bitrate[hdr.bitrate_index][0];
			}
			else if(ver == 2)
			{
				b_rate = bitrate[hdr.bitrate_index][3];
			}
			else
			{
				// Invalid version
				return INVALID;
			}
			break;
		}
		// case layer 2
		case 2:
		{
			if(ver == 1)
			{
				b_rate = bitrate[hdr.bitrate_index][1]; 
			}
			else if(ver == 2)
			{
				b_rate = bitrate[hdr.bitrate_index][4];
			}
			else
			{
				// Invalid version
				return INVALID;
			}
			break;
		}
		//case layer 3
		case 3:
		{
			if(ver == 1)
			{
				b_rate = bitrate[hdr.bitrate_index][2]; 
			}
			else if(ver == 2)
			{
				b_rate = bitrate[hdr.bitrate_index][4];
			}
			else
			{
				// Invalid version
				return INVALID;
			}
			break;
		}
		default:
		{
			return INVALID;
		}
	}
	return b_rate;
	
}


/**
** calculates the size of the frame in bits
** hdr is the header read by frameInfo fn
**/
int frameSize(FRAMEHEADER hdr)
{
	if(hdr.framesync != 0x7ff)
	{
		return ERROR;
	}	
	// stores all the equivalent information
	int padding = hdr.padding;
	int ver = version[hdr.version];
	int frequency = sample_rate[hdr.sample_rate][ver];
	int b_rate = 0;
	int lyr = layer[hdr.layer];
	// returns negative on unsupported versions or/and layers
	if((b_rate = bitrateVal(hdr)) < 0)
	{
		return b_rate;
	}
	// returns the calculated size according to the layer without the header
	if(lyr == 1)
	{
		return ((12 * b_rate / frequency + padding) * 4) - 4;
	}
	else
	{
		return (144 * b_rate / frequency + padding) - 4;
	}	
}

/**
** calculates the rate of the frame
** hdr is the header read by frameInfo fn
**/

LONG rate(FRAMEHEADER hdr, int size)
{
	
	// check for valid MP3 Frame
	if(hdr.framesync != 0x7ff)
	{
		return ERROR;
	}	
	// Stores the appropriate samples/frame ratio
	int ver = version[hdr.version];
	int lyr = layer[hdr.layer];
	int samples = samples_frame[lyr][ver];
	int samp_rate = sample_rate[hdr.sample_rate][ver]; 
	
	// checks for valid sample
	if(samples == RESERVED)
	{
		return ERROR;
	}
	// frequency is in how much nanoseconds for the current frame
	float tns = ((float) samples/samp_rate) * 1000000000;
	return (LONG) tns;
	
}

/**
**
** Traverse through the current Mp3 Audio Frame 
** The FILE* pointer must be positioned at the begining of the Current Audio Frame
**/
int traverse(FILE* media, int* frm_count, int* frq, buffer* buff)
{
	FRAMEHEADER hdr;
	
	// reads the frame header 
	if(frameInfo(media, &hdr) == EOF)
	{
		return EOF;
	}
	// calculates the size
	int size = frameSize(hdr);	

	// tells the current position of the file before seeking
	int position = ftell(media) - 4;

	fseek(media, -4, SEEK_CUR);

	//reads the data and buffers it 
	BYTES segment = malloc(sizeof(char) * (size + 4));
	fread(segment, size + 4, 1, media);
	addBuffer(buff, segment, size + 4);



	LONG time = rate(hdr, size); // in nano seconds 1x10^9
	// Checks for INVALID
	if(size == INVALID || time == INVALID)
	{
		return INVALID;
	}
	// returns End of file on error
	else if(size == ERROR || time == ERROR)
	{
		return EOF;
	}

	// Duration of the current frame
	struct timespec t;
		t.tv_sec = time / 1000000000;
		t.tv_nsec = time % 1000000000;

	//sleeps if the fastforward flag is off
	if(fastforward == false)
	{
		// sleeps for the appropriate amount of time 
		nanosleep(&t,&t);
	}
	// will not sleep for 800 frames
	else
	{
		if(forward_count == 800)
		{
			fastforward = false;
			forward_count = 0 ;
		}
		else
		{
			forward_count++;
		}
		
	}
	*frm_count = ++frame_count;

	
	
	// Frames / second
	*frq = (int) 1000000000/t.tv_nsec; 		
	return position;	
}


/*
* returns a mp3 frame header with the given info
*/
uint32_t createFrameHeader(FRAMEHEADER hdr)
{
	uint32_t header = 0xFFFFFFFF;

	// setting the version
	int tmp = hdr.version;
	header = header & (tmp << 11);
	
	//setting the layer number
	tmp = hdr.layer;
	header = header & (tmp << 13);

	// setting the protection bit 
	tmp = hdr.protection;
	header = header & (tmp << 15);

	//setting the bitrate index
	tmp = hdr.bitrate_index;
	header = header & (tmp << 16);

	//setting the sample rate
	tmp = hdr.sample_rate;
	header = header & (tmp << 20);

	// setting the padding bit
	tmp = hdr.padding;
	header = header & (tmp << 22);

	//setting the private bit 
	tmp = hdr.private_bit;
	header = header & (tmp << 23);
	
	//setting the cahnnel mode 
	tmp = hdr.channels;
	header = header & (tmp << 24);


	//setting the extension
	tmp = hdr.extension;
	header = header & (tmp << 26);

	//setting the copy right bits
	tmp = hdr.cpy_right;
	header = header & (tmp << 28);

	// setting the Original bit
	tmp = hdr.original;
	header = header & (tmp << 29);

	//setting the original 
	tmp = hdr.original;
	header = header & (tmp << 30);

	return htonl(header);
}


BYTES createFrames(char* buffer, FRAMEHEADER hdr, int totalsize, int nFrames)
{
	// size of audio in one frame
	int size = totalsize / nFrames;

	// header data
	int header = createFrameHeader(hdr);

	// total number of bytes
	int n =  (totalsize + (4 * nFrames));
	BYTES frames = malloc(sizeof(BYTES) * n); // 4 is the size of the frame header 

	if(frames == NULL)
	{
		return NULL;
	}

	// counter for the buffer array
	int ctr = 0;
	// divides the audio into frames 
	for(int i = 0; i < n; i = i + size + 4)
	{
		// writes the frame headers at the begginig of each frame
		frames[i]  = 0xFF;
		frames[i] = frames[i] & header;
		frames[i + 1] = 0xFF;
		frames[i + 1] = frames[i + 1] & (header >> 8);
		frames[i + 2] = 0xFF;
		frames[i + 2] = frames[i + 2] & (header >> 16);
		frames[i + 3] = 0xFF;
		frames[i + 3] = frames[i + 3] & (header >> 24);

		// stores the audio according to the size after the header
		for(int j = i + 4; j < size; j++)
		{
			frames[j] = buffer[ctr++]; 
		}
	}

	// hopefully it was successfull
	return frames;
}

/*
* takes a pcm samples and encodes the as mp3 frames
*
* adapted from https://stackoverflow.com/a/2496831/7280848
*/
int encode_PCM(buffer* buff, BYTES* encoded)
{
	// initialize lame encoder 
	lame_t lame = lame_init();
	lame_set_in_samplerate(lame, 44100); // make sure to change in case of any changes in capture 
	lame_set_VBR(lame, vbr_default);
	lame_init_params(lame);

	
	// encodes the mp3 buffer
	short* pcm_buffer = (short*) toBYTES(buff);
	unsigned char* mp3_buffer = malloc(sizeof(char) * BUFFER_SIZE);

	int size = lame_encode_buffer_interleaved(lame, pcm_buffer, BUFFER_SIZE * 2 , mp3_buffer, BUFFER_SIZE);
	//lame_encode_flush(lame, mp3_buffer, BUFFER_SIZE);

	// free all the resources	
	free(pcm_buffer);
    lame_close(lame);
    *encoded = mp3_buffer;
    return size;
}
