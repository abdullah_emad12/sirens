/*
*  Saleh, A. E. (2017, May & june). LinkedList (Version 1) [Computer software].
*
* A double linkedlist that stores all the files in the playlist that will be played by broadcast
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ERROR -1
#define BOLD  "\033[01m"
#define NORMAL  "\033[00m"
#define GREEN	"\033[32m"


#include "headers/linkedlist.h"
/**
** Creates an Empty linked list
**/
DLL* create_dll()
{
	// allocates size
	DLL* list = malloc(sizeof(DLL));
	
	//checks for failure	
	if(list == NULL)
	{
		return NULL;
	}
	
	// sets variables
	list->head = NULL;
	list->size = 0;
	list->current = NULL;
	return list;
}

/**
** adds an element to the linked list
**/
int add(char* fname, DLL* list)
{
	// checks if the linked list has not been created yet
	if(fname == NULL || list == NULL)
	{
		return ERROR;	
	}
	
	// allocates memory and checks for errors
	node* element = malloc(sizeof(node));
	element->next = NULL;
	element->previous = NULL;
	if(element == NULL)
	{
		return ERROR;
	}
	
	// copies the char*
	strcpy(element->title, fname);
	
	// inserts in the head if first element
	if(list->head == NULL)
	{
		list->head = element;
		list->size++;
		list->current = list->head;
		return 0;
	}
	node* cur = list->head;
	
	// traverse through the list
	while(cur->next != NULL)
	{
		cur = cur->next;
	}

	cur->next = element;
	element->previous = cur;
	list->size++;
	return 0;
}

/**
** goes forward one element from the current position
**/
int forward(DLL* list)
{
	// checks for empty list
	if(list == NULL || list->head == NULL)
	{
		return ERROR;
	}
	if(list->current == NULL)
	{
		return EMPTY;
	}
	// checks for last item
	else if(list->current->next == NULL)
	{
		return EMPTY;
	}
	
	// goes forward	
	list->current = list->current->next;
	if(list->current->next == NULL)
	{
		return 0;
	}
	return 1;
}

/**
** goes backward one element from the list
**/
int backward(DLL* list)
{
	// checks for empty list
	if(list == NULL || list->head == NULL)
	{
		return ERROR;
	}
	// checks for last item
	if(list->current->previous == NULL)
	{
		return EMPTY;
	}
	
	// goes forward	
	list->current = list->current->previous;
	return 0;
}

/**
** returns an array of char(s) with the name of the current mp3 file
**/
int getCurrent(DLL* list, char* song)
{
	// checks for safety	
	if(list == NULL)
	{
		return ERROR;
	}
	strcpy(song, list->current->title);
	return 0;
}

/**
** extracts the list from the text file
**/
DLL* extract_list(char* name)
{
	// list that will be returned
	DLL* list = create_dll();



	FILE* txt = fopen(name, "r");
	if(txt == NULL)
	{
		return NULL;
	}
	char c;
	char buffer[50];
	memset(buffer, 0, 50);
	// reads line by line and stores it in buffer
	for(int i = 0; (c = (char)fgetc(txt)) != EOF; i++)
	{
		if(c != ' ')
		{
			// if a new line terminates buffer
			if(c == '\n')
			{
				// allocates a new space, copies the string and adds it to the list
				if(strstr(buffer, ".mp3") != NULL)
				{	
					add(buffer, list);
					memset(buffer, 0, 50);
					i = -1;
				}
			}
			else
			{
				buffer[i] = c;
			}
		}
		else
		{
			i--;
		}
	}
	// checks if the file does not end with a new line
	if(buffer[0] != 0)
	{
		if(strstr(buffer, "mp3") != NULL)
		{
			add(buffer,list);	
		}
	}
	fclose(txt);
	return list;
}
/**
** returns the size of the list
**/
int lsize(DLL* list)
{
	return list->size;
}

/**
** prints all the elements in the list with the current bolded
**/
int printl(DLL* list)
{
	// checks for Empty list
	if(list == NULL)
	{
		return ERROR;
	}
	if(list->head == NULL)
	{
		printf("Empty List\n");
	}
	else
	{
		printf("%d Items were found in the list\n",list->size);
		node* temp = list->head;
		for(int i = 0 ; i < list->size; i++)
		{
			if(temp == list->current)
			{
				printf("%s%s", GREEN, BOLD);
				printf("%d. %s\n",i + 1 , temp->title);
				printf("%s",NORMAL);
			}
			else
			{
				printf("%s%d. %s\n", NORMAL, i + 1, temp->title);
			}
			fflush(stdout);
			temp = temp->next;
		}
	}
	return 0;
	
	
}


/**
** Free all the allocated memory
**/
void destroy_helper(node* trav)
{
	if(trav == NULL)
	{
		return;
	}
	destroy_helper(trav->next);
	free(trav);
	
}

void destroy(DLL* list)
{
	if(list->head == NULL || list == NULL)
	{
		return;
	}
	destroy_helper(list->head);
	free(list);
}
