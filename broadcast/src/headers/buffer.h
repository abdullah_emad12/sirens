/*
* Saleh, A. E. (2017, May & june). Buffer (Version 1) [Computer software].
*
* Responsible for buffering MP3 data in the memory for transmision
*/

typedef unsigned char* BYTES;

/*
* The buffer structure
*/
typedef struct  frame_buffer 
{
	BYTES data; // a MP3 frame will be stored here
	struct frame_buffer* next;	// points to the next node
	int size; // size of the frame
}frame_buffer;

typedef struct buffer
{
	frame_buffer* head; // points to the first node in the buffer
	frame_buffer* tail; // points to the last node in the buffer
	int size; // the total size of the buffer in bytes 

}buffer;

// prototypes 

/*
* Creates a new buffer
*/
buffer* createBuffer();



/*
* Adds the a frame to the last node
*/

int addBuffer(buffer* buf, BYTES segment, int size);


/*
* concatenates all the bytes together
*/
BYTES toBYTES(buffer* buf);


/*
* Frees all the memory used by the buffer
*/
void destroyBuffer(buffer* list);