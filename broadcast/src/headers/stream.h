/*
*
* Saleh, A. E. (2017, May & june). Stream (Version 2) [Computer software].
*
* All the functions that read and write stream of data
* 
*/



// super global variable
extern bool terminate;
extern int requests;
extern char state;
typedef char* string;

/**
** Arguments that will be passed between thread 1 and thread 2
**/
typedef struct traverse_thread_args
{
	int minutes;
	int seconds;
	int frq;
	int frm_count;
	FILE* media;
} traverse_thread_args;



// prototypes
string getString(void);
int arrString(char* in);
int getLocation(char* dir, char* mp3,char* location);
__mode_t perChange(string path);
void updateState();
void resetState();
void send_buffer(unsigned char* segment, int size, char* header);



