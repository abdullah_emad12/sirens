/*
* Saleh, A. E. (2017, May & june). mpg (Version 1) [Computer software].
*
* Made for reading and interpreting MP3 Files Mainly
*
* MP3 related data type
* 
* Adapted from http://mpgedit.org/mpgedit/mpeg_format/mpeghdr.htm
* 
* This is ID3V2 Header strcuture 
*
* Adapted from http://id3.org/id3v2.4.0-structure 
*
*/

// includes
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "buffer.h"


// Global Flags
extern bool fastforward;


//constants
#define reservered 0
#define layerIII 1
#define layerII 2
#define layerI 3
#define FREE 0xfffe
#define BAD 0xffff 
#define RESERVED 0xffff
#define INVALID -555
#define ERROR -12


// typedefs
typedef uint8_t  BYTE;
typedef int32_t DWORD;
typedef uint32_t  LONG;
typedef uint16_t WORD;

/**
** MPEG frame header.
** it constitute every first four bytes(32-bits) in a frame
**/
  
typedef struct
{
	WORD framesync; //frame sync (all bits set)
	BYTE version; // MPEG Audio version ID
	BYTE layer; // layer description
	BYTE protection; // Protection bit
	BYTE bitrate_index; //Bitrate index
	BYTE sample_rate; // sampling rate frequence index (values are in Hz)
	BYTE padding; // Padding bit
	BYTE private_bit; //private bit
	BYTE channels; // Channel mode
	BYTE extension; // Mode extension(Only if Joint stereo)
	BYTE cpy_right; // Audio is Copy righted
	BYTE original; // Original Copy
	BYTE emphasis; // Emphasis 
}__attribute__((__packed__))
FRAMEHEADER;


/**
** TAG is always present at the beggining of a ID3V2 MP3 file 
** Constant size 10 bytes
**/
typedef struct
{
	BYTE id[3]; //"ID3"
	WORD version; // $04 00
	BYTE flags; // %abcd0000
	LONG size; //4 * %0xxxxxxx
}__attribute__((__packed__))
ID3TAG;


/**
** The extended header may contain information that provides more insight to the mp3 file
** Optional; however 6 bytes at least must be present
**
**/
typedef struct
{
	//size of the extended header
	LONG size; //4 * %0xxxxxxx
	BYTE flags_bytes; //$01
	BYTE extended_flags; //xx
	
	
}EXTENDEDHEADER;


/**
**	The frame header that is followed by data about the MP3 file
**
**/

typedef struct
{
	LONG id; //xx xx xx xx
	LONG size; //4 * %0xxxxxxx
	char flags[2]; //xx xx
}INFOFRAME;



/**
** The footer maybe added to the end of the file
**/
typedef struct
{
	BYTE id[3]; //"3DI"
	WORD version; // $04 00
	BYTE flags; // %abcd0000
	LONG size; //4 * %0xxxxxxx
	
}FOOTER;




/** 
** This TAG is used to describe the MPEG audio file
** It contains information about artist, title, album, publishing year and genre.
** It is exactly 128 bytes long and is located at very end of the audio data.
**
**/

typedef struct
{
	char tag[3]; //Tag identification.Must contain 'TAG' if tag exists
	char title[30]; // Audio title
	char artist[30]; // name of the artist
	char album[30]; //album
	char year[4]; //year
	char comment[30];// if any comments
	char genre; //Genre
	
}ID3V1;


// prototypes

/*
 * Makes sure the file is supported and returns the correct size
 * returns -1 on errors or the size of the ID3V2 Tag
 */

int mp3Header(FILE* media);

/*
 *  Looks for all the information related to the song 
 *	writes the name of the song and artist to the passed char* and returns 0 on success
 */
int songInfo(FILE* media ,char* song, char* artist);

/*
 * writes time and size related info to the given variables 
 * media is the MP3 file that will be read
 * ms is the bits/second rate at which the MP3 will be played
 */
int frameInfo(FILE* media, FRAMEHEADER* hdr);

/*
 * Traverse through the current Mp3 Audio Frame 
 * The FILE* pointer must be positioned at the begining of the Current Audio Frame
 */
int traverse(FILE* media, int* frm_count, int* frq, buffer* buff);


/*
 * Finds the location of the first MP3 frame, if exists
 */
int Framesync_lookup(FILE* media);


/*
* takes a pcm samples and encodes the as mp3 frames
*
* adapted from https://stackoverflow.com/a/2496831/7280848
*/
int encode_PCM(buffer* buff, BYTES* encoded);


