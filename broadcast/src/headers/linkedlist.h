/*
*  Saleh, A. E. (2017, May & june). LinkedList (Version 1) [Computer software].
*
* A double linkedlist that stores all the files in the playlist that will be played by broadcast
*/



// constants
#define EMPTY -2

typedef struct node
{
	char title[50];
	struct node* previous;
	struct node* next;
}node;

typedef struct DLL
{
	node* head;
	node* current;
	int size;
}DLL;

// prototypes

/**
** Creates an Empty linked list
**/
DLL* create_dll();


/**
** adds an element to the linked list
**/
int add(char* fname, DLL* list);


/**
** goes forward one element from the current position
**/
int forward(DLL* list);


/**
** goes backward one element from the list
**/
int backward(DLL* list);


/**
** returns an array of char(s) with the name of the current mp3 file
**/
int getCurrent(DLL* list, char* song);


/**
** extracts the list from the file
**/
DLL* extract_list(char* name);

/**
** prints all the elements in the list with the current bolded
**/
int printl(DLL* list);


/**
** Free all the allocated memory
**/
void destroy(DLL* list);

/**
** returns the size of the list
**/

int lsize(DLL* list);
