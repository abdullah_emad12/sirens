/*
* Saleh, A. E. (2017, May & june). Capture (Version 1) [Computer software].
*
* captures and buffers MP3 Frames from the sound Driver
*
* Credit for some of the code present in this file goes to 
*
* http://www.linuxjournal.com/article/6735
* http://www.alsa-project.org/alsa-doc/alsa-lib/pcm_2pcm_8c.html
*
*/

#include <stdbool.h>




//constants
#define SAMPLERATE 44100 // CD quality
#define PERIODSIZE 32
#define BUFFER_SIZE 8192

//global variables
extern bool record;

// name of the broadcaster and his show
extern char username[30];
extern char showtitle[50];

// definitions
typedef unsigned char* BUFFER;


//prototypes 

/*
* prepares the hardware for sound capture
*/
int create_session();





/*
* start capturing sound from the sound driver and saving it as a buffer
*/

int start_session();




/*
* frees all the memory and stop recording 
*/
void end_session();

