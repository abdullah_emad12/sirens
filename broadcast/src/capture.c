/*
* Saleh, A. E. (2017, May & june). Capture (Version 1) [Computer software].
*
* captures and buffers MP3 Frames from the sound Driver
*
*/

// macros
#define ALSA_PCM_NEW_HW_PARAMS_API


// includes
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <lame/lame.h>


// includes
#include <time.h>
#define timespec linux_timespec
#include <alsa/asoundlib.h>
#undef timespec


// headers
#include "headers/capture.h"
#include "headers/stream.h"
#include "headers/mpg.h"

// prototypes
void printT(int* seconds, int* minutes);
char* broadcastInfo(void);


/*
* All the components that will be used to read data from the driver
*/
typedef struct pcm
{
	snd_pcm_t* handle; // the hardware handler
	snd_pcm_hw_params_t* params; // the paramaters of the hardware
	snd_pcm_uframes_t frames; // pcm Frames
	unsigned int period; // the current time since the recording started
	BUFFER buffer; // will hold the audio data
	unsigned int size; // size of the buffer
}pcm;


// global variables
pcm session;




/*
* prepares the hardware for sound capture
*/
int create_session()
{
	int err; // if any error occurs 

	int dir;

	// attempts to open the PCM device and checks for errors
	err = snd_pcm_open(&(session.handle), "default",SND_PCM_STREAM_CAPTURE, 0);
	if(err < 0)
	{
		printf("unable to open PCM device\nDetails: %s\n", snd_strerror(err));
		return err;
	}

	// allocates memory for the hardware parameters
	err = snd_pcm_hw_params_malloc (&(session.params));
	if(err < 0)
	{
		printf("Unable to allocate memory for the hardware parameters\nDetails: %s\n", snd_strerror(err));
		return err;
	}

	// sets the default parameters for the hardware
	err = snd_pcm_hw_params_any(session.handle, session.params);
	if(err < 0)
	{
		printf("Unable to set the default Hardware Parameters\nDetails: %s\n", snd_strerror(err));
		return err;
	}


	/* Customize some hardware parameters */

	// Interleave mode
	snd_pcm_hw_params_set_access(session.handle, session.params, SND_PCM_ACCESS_RW_INTERLEAVED);


	// sets the endianess that will be suitable to convert the following to mp3 frames
	snd_pcm_hw_params_set_format(session.handle, session.params, SND_PCM_FORMAT_S16_LE);

	// two channels steroe
	snd_pcm_hw_params_set_channels(session.handle, session.params, 2);

	// sample rate 44100 bits/seconds
	unsigned int tmp = SAMPLERATE; 
	snd_pcm_hw_params_set_rate_near(session.handle, session.params, &tmp, &dir);

	//period size 
 	session.frames = PERIODSIZE;
  	snd_pcm_hw_params_set_period_size_near(session.handle, session.params, &(session.frames), &dir);

  	// after setting the parameters we want to write it to the drive
 	 err = snd_pcm_hw_params(session.handle, session.params);
 	 if (err < 0) 
 	 {
 	 	printf("Unable to set the paramater to the drive\nDetails: %s\n", snd_strerror(err));
 	   return err;
 	 }

  	// calculating the period size to allocate enough memory for buffer
  	snd_pcm_hw_params_get_period_size(session.params, &(session.frames), &dir);
  	session.size = session.frames * 4; // number of frames * 2 bytes * 2 channels

  	// buffer
  	session.buffer = malloc(sizeof(char) * session.size);

	// set the period of the session to 3 seconds
	snd_pcm_hw_params_get_period_time(session.params, &(session.period), &dir);

	return 0;
}

/*
* start capturing sound from the sound driver and saving it as a buffer
*/

int start_session()
{
	int err;
	// holds 3 seconds of buffer
	buffer* holder = createBuffer();
	// keeps looping until the broadcaster terminates it 
	int time = 0; 
	int seconds = 0;
	int minutes = 0;
	while(record)
	{
		int totalSize = session.frames * 4;
		// reads 32 frames and stores them into buffer
		err = snd_pcm_readi(session.handle, session.buffer, session.frames);

		// checks for overrun
		if(err == -EPIPE)
		{
			fprintf(stderr, "%s\n", snd_strerror(err));
			snd_pcm_prepare(session.handle);
		}
		//other errors
		else if(err < 0)
		{
			fprintf(stderr, "Error While capturing sound \nDetails: %s\n", snd_strerror(err));
		}
		// underrun
		else if(err != PERIODSIZE)
		{
			fprintf(stderr, "Short read. Frames number: %d \nDetails: %s\n",err, snd_strerror(err));
		}

		// adds a new buffer
		BYTES buffer = malloc(sizeof(char) * totalSize);
		memcpy(buffer, session.buffer, totalSize);
		addBuffer(holder, buffer, totalSize);

		// holder holds enough buffer to be encoded
		if(holder->size == BUFFER_SIZE * 7)
		{
			// sends this chumck to the server
			BYTES segment = NULL;
			int segment_size = encode_PCM(holder, &segment);
			char* header = broadcastInfo();
			send_buffer(segment, segment_size, header);
			if(header != NULL)
			{
				free(header);
			}

			// frees the buffers and 
			free(segment);
			destroyBuffer(holder);
			holder = createBuffer();
		}
		// updates the total time
		if(time % (1000000 / session.period) == 0)
		{
			printT(&seconds, &minutes);
		}
		time++;

	}
	if(holder != NULL)
	{
		destroyBuffer(holder);
	}
	return 0;
}

/*
* frees all the memory and stop recording 
*/
void end_session()
{
	if(session.handle != NULL)
	{
		snd_pcm_drain(session.handle);
 		snd_pcm_close(session.handle);	
	}
	if(session.buffer != NULL)
	{	
		free(session.buffer);
	}
 
}

/*
* Prints and updates a given time
*/
void printT(int* seconds, int* minutes)
{
	if(*seconds == 60)
	{
		(*minutes)++;
		*seconds = 0;
	}
	printf("\033[2J"); // clears the screan
	printf("\033[%d;%dH", 5, 0); // centers the text
	printf("\033[32m"); // foreground color green 
	printf("On Air .......\n");
	printf("\ntime elapsed: %02d:%02d\n", *minutes, (*seconds)++);
	printf("\033[39m"); // returns to the default color
	fflush(stdout);
}

/*
* header info 
*/
char* broadcastInfo(void)
{
	// Allocates enough memory for the header that will be sent to the PHP program	
	char* buffer = malloc(sizeof(char) * (strlen(username) + strlen(showtitle) + 5));

	// checks if memory allocation failed	
	if(buffer == NULL)
	{
		return NULL;
	}

	// Writes info to buffer
	sprintf(buffer, "%s\n%s\n", username, showtitle);
	return buffer;
}