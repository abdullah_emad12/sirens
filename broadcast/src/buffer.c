/*
* Saleh, A. E. (2017, May & june). Buffer (Version 1) [Computer software].
*
* Responsible for buffering MP3 data in the memory for transmision
*/




#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "headers/buffer.h" 


/*
* Creates a new buffer
*/
buffer* createBuffer()
{
	buffer* buf = malloc(sizeof(buffer));

	// checks for failures
	if(buf == NULL)
	{
		return NULL;
	}
	buf->size = 0;
	buf->head = NULL;
	buf->tail = NULL;
	return buf;
}

/*
* Adds the a frame to the last node
*/

int addBuffer(buffer* buf, BYTES segment, int size)
{
	// checks for NULL arguments
	if(buf == NULL || segment == NULL)
	{
		return -1;
	}

	// creates a new frame
	frame_buffer* fr = malloc(sizeof(frame_buffer));
	if(fr == NULL)
	{
		return -1;
	}
	fr->next = NULL;
	fr-> data = segment;
	fr->size = size;
	// inserts in the head if first element
	if(buf->head == NULL)
	{
		buf->head = fr;
		buf->size += size;
		buf->tail = buf->head;
		return 0;
	}

	// insert to the last element in the list
	buf->tail->next = fr;
	buf->tail = fr;
	buf->size += size;
	return 0;
}

/*
* concatenates all the bytes together
*/
BYTES toBYTES(buffer* buf)
{
	BYTES total = malloc((sizeof(BYTES) * buf->size) + 1);
	int index = 0;
	frame_buffer* curr = buf->head;
	//iterate over every frame
		int size = 0;
	while(curr != NULL)
	{
		// copies the context of the frame to totat
		for(int i = 0; i < curr->size; i++)
		{
			total[index++] = curr->data[i];
			size++;
		}
		curr = curr->next;
	}
	return total;
}

/*
* destroys every frame independtly
*/
void destroyBuffer_helper(frame_buffer* fr)
{
	if(fr == NULL)
	{
		return;
	}
	destroyBuffer_helper(fr->next);
	if(fr->data != NULL)
	{
		free(fr->data);
	}
	free(fr);
}

/*
* Frees all the memory used by the buffer
*/
void destroyBuffer(buffer* list)
{
	if(list->head == NULL || list == NULL)
	{
		return;
	}
	destroyBuffer_helper(list->head);
	free(list);
}

