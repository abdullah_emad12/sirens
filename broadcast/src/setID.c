/*
* Saleh, A. E. (2017, May & june). setID (Version 1) [Computer software].
*
* This program reads a Mp3 binary file and checks if it has an ID3V1 header at the end of the file
* Adds the header with the neccessary info if it doesn't
*/


// Macros
#define _POSIX_C_SOURCE 200809L

// definitions
typedef char* string;

// libraries

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// Customized Headers
#include "headers/mpg.h"


// prototypes
int arrString(char* in, int n);


int main(int argc, char* argv[])
{
	// checks for the correct no. of arguments
    if(argc != 2)
    {
        printf("USAGE: ./setID file.mp3\n");
        return 1;    
    }
    
    string file = argv[1];
    // the file name doesn't end in mp3 
    if(strstr(file, ".mp3") == NULL)
    {
    	printf("Couldn't read file\n");
    	return 2;
    }

   	FILE* fileptr = fopen(file, "a+");

   	// checks for availability
   	if(fileptr == NULL)
   	{
   		printf("Couldn't read file\n");
   		return 3;
   	}

   	fseek(fileptr, 0, SEEK_SET);

   	// Makes sure the provided file is a valid mp3
   	if(Framesync_lookup(fileptr) == ERROR)
   	{
   		printf("invalid MP3 file\n");
   		return 4;
   	}

   	// Makes sure the MP3 does not already contain the ID3V1 header
   	char temp1[30];
   	char temp2[30];
   	if(songInfo(fileptr, temp1, temp2) == 0)
   	{
   		printf("The file already contains a header.\nTitle: %s \nArtist: %s\n", temp1, temp2);
   		return 5;
   	}

   	// info
   	char* id = malloc(sizeof(char) * 3);
   	char* title = malloc(sizeof(char) * 30);
   	char* artist = malloc(sizeof(char) * 30);
   	char* album = malloc(sizeof(char) * 30);
   	char* year = malloc(sizeof(char) * 4);
   	char* comments = malloc(sizeof(char) * 30);
   	char* genre = malloc(sizeof(char) * 1);

   	memset(title, 0, 31);
   	memset(artist, 0, 31);
   	memset(album, 0, 31);
   	memset(year, 0, 5);
   	memset(comments,0 ,31);
   	memset(genre, 0, 2);

   	//Sets the info 

   	id[0] = 'T';
   	id[1] = 'A';
   	id[2] = 'G';


   	printf("The title:\t(Max 30 characters)\n");

   	arrString(title, 31);

   	printf("The Artist:\t(Max 30 characters)\n");

   	arrString(artist, 31);

   	printf("The album:\t(Max 30 characters)\n");

   	arrString(album, 31);

   	printf("The Year:\t(Max 4 characters)\n");

   	arrString(year, 5);

   	printf("Any comment:\t(Max 30 characters)\n");

   	arrString(comments, 31);

   	printf("The genre:\t(Max 1 characters)\n");

   	arrString(genre, 2);

   	// gets the number instead of the ascii character
   	genre[0] = atoi(genre);

   	char response[4];
   	do
   	{
   		printf("Are you sure you want to proceed? Changes can't be undone (yes/no)\n");
   		arrString(response, 4);
   	}while(strcmp(response, "yes") != 0 && strcmp(response, "no") != 0 && strcmp(response, "y") != 0 && strcmp(response, "n") != 0);

   	if(strcmp(response, "no") == 0 || strcmp(response, "n") == 0)
   	{
   		free(id);
	   	free(title);
	   	free(artist);
	   	free(album);
	   	free(year);
	   	free(comments);
	   	free(genre);
	   	fclose(fileptr);
   		printf("Exiting Program ...\n");
   		return 5;
   	}

   	fseek(fileptr, 0, SEEK_END);

   	// write data to the file
   	fwrite(id, 1, 3, fileptr);
   	fwrite(title, 1, 30, fileptr);
   	fwrite(artist, 1, 30, fileptr);
   	fwrite(album, 1, 30, fileptr);
   	fwrite(year, 1, 4, fileptr);
   	fwrite(comments, 1, 30, fileptr);
   	fwrite(genre, 1, 1, fileptr);


  

   	printf("Process completed successfully!\n\nTitle: %s\nArtist: %s\nAlbum: %s\nYear: %s\ncomment: %s\n", title, artist, album, year, comments);


   	// frees all the memory to avoid leaks

   	free(id);
   	free(title);
   	free(artist);
   	free(album);
   	free(year);
   	free(comments);
   	free(genre);





  
   	fclose(fileptr);
}

/**
** Stores an input from the keyboard in an array of char 
** Maximum size of the array is n
**/
int arrString(char* in, int n)
{	
	fflush(stdin);
	char c = 0;
	int size = 0;
	if(in == NULL)
	{
		return -1;	
	}
	while((c = fgetc(stdin)) != EOF && c != '\n')
	{
		// if the user attempts to enter more than 30 characters
		if(size == n - 1)
		{
			in[n - 1] = '\0';
			return size;
		}
		
		// stores the character in the array
		in[size++] = c;
	}
	in[size]= '\0';
	// success
	return size;
}
