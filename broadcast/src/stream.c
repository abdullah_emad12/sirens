/*
*
* Saleh, A. E. (2017, May & june). Stream (Version 2) [Computer software].
*
* All the functions that read and write stream of data
* 
*/


// macro requirement
#define _POSIX_C_SOURCE 200809L

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#include <string.h>

// Change permission requirements
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <signal.h>
// FIFO requirements
#include <fcntl.h>

// headers
#include "headers/mpg.h"
#include "headers/stream.h" 
#include "headers/linkedlist.h"

// super global
char state = -1;

/**
** changes the permssions of the file to allow Group and others to read 
**/
__mode_t perChange(string path)
{
	struct stat buff;
	
	// grabs informatio about the file
	if(stat(path, &buff) != 0)
	{
		return ERROR;
	}
	mode_t st_mode = buff.st_mode;
	
	// extracts the first 9 bits only	
	mode_t temp = st_mode & 0x1FF;
	
	// reads the permission requirements
	/*int mode = st_mode.S_IRUSR |  st_mode.S_IWUSR | st_mode.S_IXUSR | st_mode.S_IRGRP | st_mode.S_IWGRP | st_mode.S_IXGRP | st_mode.S_IROTH | st_mode.S_IWOTH | st_mode.S_IXOTH;*/

	// returns zero if permissions are set 
	if(temp == 0644)
	{
		return 0;	
	}
	// clears the first 9 bits
	mode_t mode = st_mode;
	st_mode = st_mode >> 9;
	st_mode = st_mode << 9;
	st_mode = st_mode | 0644;
	
	// set's the new permissions	
	if(chmod(path, st_mode) != 0)
	{
		return ERROR;
	}
	// returns the old permissions
	return mode;
	
}



/**
**Gets a string from the user
** this function is written based on the cs50 library cs50.h
**/
string getString(void)
{
	int size = 0;
	int capacity = 0;
	char c = 0;
	string buffer = NULL;
	// keeps waiting for input until the user press enter or the EOF is reached
	while((c = fgetc(stdin)) != EOF && c != '\n')
	{
		if(size > capacity - 1)
		{
			if(capacity == 0)
			{
				capacity = 32;
			}
			// doubles the capacity every time we run out of space
			else
			{
				capacity = capacity * 2;
			}
			string temp = realloc(buffer, sizeof(char) * capacity);
			if(temp == NULL)
			{
				free(buffer);
				return NULL;
			}
			buffer = temp;
		}
		buffer[size++] = c;
	}

	// copies the input string to the output string
	string out = malloc((1 + size) * sizeof(char));
	strncpy(out,buffer,size);
	if(buffer != NULL)
	{	
		free(buffer);
	}
	// terminates the string
	out[size] = '\0';
	return out;
}
/**
** Stores an input from the keyboard in an array of char 
** Maximum size of the array is 30
**/
int arrString(char* in)
{	
	char c = 0;
	int size = 0;
	if(in == NULL)
	{
		return -1;	
	}
	while((c = fgetc(stdin)) != EOF && c != '\n')
	{
		// if the user attempts to enter more than 30 characters
		if(size == 28)
		{
			in[29] = '\0';
			return size;
		}
		
		// stores the character in the array
		in[size++] = c;
	}
	in[size]= '\0';
	// success
	return size;
}
/**
** Gets the location of the Mp3 file and stores it in location
**/
int getLocation(char* dir, char* mp3, char* location)
{

		if(strlen(mp3) == 0 || location == NULL)
		{
			return -1;
		}
		
		// checks if the user choose the current
		if(strlen(dir) == 0)
		{
			char cur_dir[512];
	
			// gets the current directory and checks for error
			if(getcwd(cur_dir, sizeof(cur_dir)) == NULL)
			{
				return -3;
			}
		
			// adds the current directory and the file name to 'location'
			sprintf(location,"%s/media/%s",cur_dir, mp3);
		}
		else
		{
			// adds the customized directory and the file name to 'location'
			sprintf(location,"/home/abdullah/%s",dir);
			// check if directory exists
			struct stat st;
			if(stat(location, &st) != 0)
			{
				return ERROR;
			}
			// concatinate the name of the file
			strcat(location, "/");
			strcat(location, mp3);
		}
		return strlen(location);
}


/*
* Increment the variable state to indicate a change and save the value of state in a hidden text file 
* Note: I wont be handeling the case where the program fails to create a new file
*/
void updateState()
{
	FILE* fstate = fopen(".state", "w");

	// stops the function on Failure
	if(fstate == NULL)
	{
		return;
	}

	// writes new data to the file indicating the new state 
	state++;
	fwrite(&state,1, sizeof(char), fstate);

	fclose(fstate);

}

/*
*  used to reset the state file to -1 
*  Usually used when the program is being terminated 
*/
void resetState()
{
	state = -1;
	FILE* fstate = fopen(".state", "w");
	// stops the function on Failure
	if(fstate == NULL)
	{
		return;
	}

	// writes new data to the file indicating the new state 
	fwrite(&state,1, sizeof(char), fstate);

	fclose(fstate);

}

/*
* Sends a signal to the node js Server and buffers the MP3 to a hidden file
*/
void send_buffer(unsigned char* segment, int size, char* header)
{
	// if the buffer was not filled
	if(segment == NULL)
	{
		return;
	}
	signal(SIGPIPE,SIG_IGN);
	// Attempts to open connection and returns INVALID on failure
	int port = open("/tmp/radio", O_WRONLY | O_NONBLOCK);
	if(port != -1)
	{	

		FILE* file = fopen("/home/abdullah/Music/buffer", "w");
		//checks the file for availability
		if(file == NULL)
		{
			return;
		}
		fwrite(segment, size, 1, file);
		fclose(file);

		// if the header was NULL
		if(header == NULL)
		{
			write(port, "read", 4);
			close(port);
			return;
		}


		// gives the signal to the server to send the chunck
		int size = strlen(header);
		
		// sends info to the PHP program
		if(write(port, &size, sizeof(int)) > 0)
		{
		
			requests++;
			write(port, header, size);
		}
		close(port);	
	}
}

	