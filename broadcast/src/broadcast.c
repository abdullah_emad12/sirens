/*
* Saleh, A. E. (2017, May & june). Broadcast (Version 3) [Computer software].
*
* Traverse through mp3 files and Write small buffers in the Music folder
* 
*  Find more information in README
*/


// Macro Requirements
#define _POSIX_C_SOURCE 2


// standard libraries
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#include <unistd.h>
// FIFO requirements
#include <sys/types.h>
#include <sys/stat.h>
#include <pthread.h>
// Pipe requirements
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#include <signal.h>
//mpg library 
#include "headers/mpg.h"
#include "headers/stream.h"
#include "headers/linkedlist.h"
#include "headers/capture.h"

//prototypes
void exit_handler(int signal);
void* keyboard_listener(void* args);
void* traverseThread(void* arg);
void* send_header(void* args);
void terminateBroadcast();
char* headerInfo();


//global variables
int cur_byte = 0;
char title[30];
char artist[30];
char location[1024];
FILE* media = NULL;
char dir[50] ;
mode_t old_perm = 0;
char username[30];
char showtitle[50];


DLL* playlist;

// FLAGS
bool terminate = false;
int requests = 0;
bool fastforward  = false; /*Global Flag*/
bool read_flag = true;
bool repeat = false;
bool lock_stdout = false;
bool record = false; // super global flag

pthread_mutex_t locker = PTHREAD_MUTEX_INITIALIZER;


// Threads 
pthread_t tds[2];


// main function
int main(int argc, char* argv[])
{
	// handles force exit to avoid memory leaks
	signal(SIGINT, exit_handler);
	string usage = "USAGE: ./broadcast -options argument file.txt\n";

	// checks for the number of arguments
	if(argc < 2)
	{
		printf("%s", usage);
		return 1;
	}
	// parses the arguemnts 
	char opt;
	while((opt = getopt(argc, argv, "hrxd:")) != -1)
	{
		switch(opt)
		{
			// directory must be inside broadcast 
			case 'd':
			{
				strcpy(dir, optarg);
				break;
			}
			// opens the manual
			case'h':
			{
				printf("Please read the manual first\n");
				system("gedit manual");
				return 3;
			}
			case 'r':
			{
				repeat = true;
				break;
			}
			case 'x':
			{
				record = true;
				break;
			}
			// unrecognized options
			case '?':
			{
				return 1;
			}
			case ':':
			{
				return 1;
			}
		}	
	}

	// checks if the name of the text file was provided
	if(argv[optind] == NULL || strlen(argv[optind]) == 0)
	{
		printf("%s", usage);
		return 0;
	}
	string file_list = argv[optind];



	// asks the user to provide his name
	printf("Provide your Full name:\t");
	while(arrString(username) < 1)
	{
		printf("Retry:");
	}

	// show name
	printf("Provide the name of the show:\t");
	while(arrString(showtitle) < 1)
	{
		printf("Retry:");
	}

	do
	{
		terminate = false;
		fflush(stdout);
		// puts all the items in the list
		
		if((playlist = extract_list(file_list)) == NULL)
		{
			printf("Unavailable text file: %s\n", file_list);
			return ERROR;
		}
		
		// checks for empty list
		if(lsize(playlist) == 0)
		{
			destroy(playlist);
			printf("Nothing to Play\n");
			return 2;
		}

		// checks if the list was not created
		if(playlist == NULL)
		{
			printf("Couldn't Create a play list %s\n",file_list);
			return 2;
		}
		// makes sure that the pipe file exists
		mkfifo("/tmp/radio", 0666);


		// starts playing music
		while(terminate == false)
		{	

			// gives permission to start reading mp3 
			read_flag = true;

			// Plays media from the list
			char song[25]; 
			getCurrent(playlist, song);

			// Gets the location of the mp3 file
			if(getLocation(dir, song, location) < 0)
			{
				printf("Couldn't locate the specified directory\n");
				terminate = true;
				continue;
			}
			

			// attempts to open the file
			FILE* media = fopen(location, "r");
				
			//checks if the file exists
			if(media == NULL)
			{
				printf("Couldn't read file\n");
				// terminates if the playlist contains no more songs
				if(forward(playlist) == EMPTY)
				{
					terminate = true;

				}
				continue;
			}
			
			// skips over the ID3V2 Tag if it exists
			if(mp3Header(media) == ERROR)
			{
				printf("Couldn't read file \n");
				if(forward(playlist) == EMPTY)
				{
						repeat = false;
				}
				continue;
			}
			// changes the permissions of the file if neccessary
			old_perm = perChange(location);
			if(old_perm == ERROR)
			{
				printf("Couldn't set The file permissions...\n");
				return ERROR;
			}
			// grabs the title and artist info from the MP3 file
			if(songInfo(media,title, artist) != 0)
			{
				printf("\nplease Enter the info manually\n\n");
				printf("Title:");

				// makes sure the title and the artist are greater than 1
				while(arrString(title) < 1)
				{
					printf("Retry:");
				}
				printf("Artist:");
				while(arrString(artist) < 1)
				{
					printf("Retry:");
				}
			}


			// Starting to broadcast a new file
			updateState();

			// arguments for the traverse thread
			traverse_thread_args data;
			data.minutes = 0;
			data.seconds = 0;
			data.frm_count = 0;
			data.media = media;
			
			
			printf("\nNow Playing %s\n\n\t", song);
			// setting attributes
			pthread_attr_t attr;
			pthread_attr_init(&attr);
			pthread_attr_t attr2;
			pthread_attr_init(&attr2);

			// creating the multiple threads
			pthread_create(&tds[0], &attr, traverseThread, &data);
			pthread_create(&tds[1], &attr2, keyboard_listener, NULL);

			// makes sure the threads are done executing
			pthread_join(tds[0], NULL);
			pthread_join(tds[1], NULL);

			// closes the Mp3 and clears the screen
			fclose(media);
			printf("\033[2J");
			printf("\033[%d;%dH", 0, 0);
			
			// plays the next song in the list
			if(forward(playlist) == EMPTY)
			{
	
				terminate = true;
			}
			// returns the old permission of the file back		
			if(old_perm != 0)
			{		
				chmod(location, old_perm);
			}
			sleep(1);
		}
		destroy(playlist);
	}while(repeat);

	terminateBroadcast();
	return 0;
}

/**
** handles forced exit
**/
void exit_handler(int signal)
{
	printf("Force closed\n");
	terminateBroadcast();
	exit(50);
}


/**
** This function Iterates over the frames of the Mp3 and updates the current byte of the frame
**
**/
void* traverseThread(void* arg)
{
	// the user wants to go live
	if(record)
	{
		// session created successfully
		if(create_session() == 0)
		{
			start_session();
		}
		end_session();

	}
	// extracts the arguments
	traverse_thread_args* info = (traverse_thread_args*) arg;
	buffer* buff = createBuffer();
	// traverse over every frame of the MP3 File
	int bufFrq = 0;
	while((cur_byte= traverse(info->media, &info->frm_count, &info->frq, buff)) >= 0 && read_flag)
	{
		pthread_mutex_lock(&locker);

		// creates new buffer every 15 frames ----- about 0.25 of second
		if(bufFrq == 15)
		{
			bufFrq = 0;
			// sends the buffer and then creates new buffer
			BYTES segment = toBYTES(buff);
			char* header = headerInfo();
			send_buffer(segment, buff->size, header);
			// avoids memory leaks
			if(header != NULL)
			{
				free(header);
			}


			// creates a new buffer
			destroyBuffer(buff);
			free(segment);
			buff = createBuffer();
		}
		bufFrq++;
		// updates the time every second according to the frequency
		if(info->frm_count % info->frq == 0)
		{
			// updates the time
			if(info->seconds == 60)
			{
				info->minutes++;
				info->seconds = 0;
			}
			if(lock_stdout == false)
			{
				// clears the screen and set the cursor
				printf("\033[2J");
				printf("\033[%d;%dH", 5, 0);
				printf("time elapsed: %02d:%02d",info->minutes, info->seconds++);
				fflush(stdout);
			}
		}
		pthread_mutex_unlock(&locker);

	}
	// cancels the other threads
	pthread_cancel(tds[1]);
	pthread_exit(0);
}

/**
** Listens to the keyboard while running for instructions
**/
void* keyboard_listener(void* args)
{
	// keeps running untill the thread is terminated
	while(true)
	{
		string command = NULL;
		command = getString();
		// force quit
		if(strcmp(command, "fq") == 0)
		{
			free(command);
			terminateBroadcast();
			exit(1);
		}
		// quits normally after the traverse is done
		else if(strcmp(command, "q") == 0)
		{
			printf("\nGot it!\n");
			terminate = true;
			repeat = false;
		}
		// prints the total number of requests
		else if(strcmp(command, "r") == 0)
		{
			// prevents traverse_thread from updating the screen
			lock_stdout = true;
			// clears the screen and prints the number of requests for 5 seconds
			printf("\033[2J");
			printf("\033[%d;%dH", 0, 0);
			printf("Number of clients requests: \t%d", requests);
			fflush(stdout);
			sleep(2);
			// returns the previous screen
			lock_stdout = false;
		}
		// prints the playlist
		else if(strcmp(command, "l") == 0)
		{
			lock_stdout = true;
			printf("\033[2J");
			printf("\033[%d;%dH", 0, 0);
			printl(playlist);
			fflush(stdout);
			sleep(2);
			lock_stdout = false;
		}
		// replays the previous song if any each time
		else if(strcmp(command,"b") == 0)
		{
			backward(playlist);
			// changes a shared variable
			pthread_mutex_lock(&locker);
			read_flag = false;
			pthread_mutex_unlock(&locker);
			updateState();

		}
		// skips one song each time
		else if(strcmp(command,"n") == 0)
		{
			// changes a shared variable
			pthread_mutex_lock(&locker);
			read_flag = false;
			pthread_mutex_unlock(&locker);
			updateState();
		}
		// fastforward 800 frames. approximately 20 seconds for most of the Mp3 files.
		else if(strcmp(command, "f") == 0)
		{
			fastforward = true;
			updateState();
		}
		else if(strcmp(command, "x") == 0)
		{
			printf("\nGot it!\n");
			record = true;
			updateState();
		}
		else if(strcmp(command, "m"))
		{
			printf("\nGot it!\n");
			record = false;
			updateState();
		}
		if(command != NULL)
		{			
			free(command);
		}
	}	
	pthread_exit(0);
}

/*
* Frees all the memory
*/
void terminateBroadcast()
{
	pthread_mutex_lock(&locker);
	if(media != NULL)
	{
		fclose(media);
	}
	if(playlist != NULL)
	{
		destroy(playlist);
	}
	if(old_perm != 0)
	{		
		chmod(location, old_perm);
	}
	unlink("/tmp/radio");
	resetState();
}

/*
* Prepares the media info that will be send to the server
*/
char* headerInfo()
{
	// Allocates enough memory for the header that will be sent to the PHP program	
	char* buffer = malloc(sizeof(char) * (strlen(location) + strlen(title) + strlen(artist) + 5));

	// checks if memory allocation failed	
	if(buffer == NULL)
	{
		return NULL;
	}

	// Writes info to buffer
	sprintf(buffer, "%s\n%s\n%s\n",  title, artist,location);	
	return buffer;
	
}