/**
** This program converts decimal number to binary numbers 
** This program was written in the purpose of debugging stream
**/


// libraries
#include <stdio.h>
#include<math.h>
#include<stdbool.h>
#include<stdlib.h>

// prototypes
bool checkalpha(char* line, long* number);
void shift(char* x, int n);

// main function
int main(int argc, char* argv[])
{
	// checks th correct number of arguments 
	if(argc != 2 )
	{
		printf("USAGE: ./dcb +integer\n");
		return 1;
	}
	// variable that holds the the input
	long decimal = 0;
	
	// checks if the variable doesnot contain any characters
 	if(!checkalpha(argv[1], &decimal))
	{
		printf("USAGE: ./dcb +integer\n");
		return 2;
	}

	// assigns suitable memory for the string with value of the binary
	int len = (int)((sizeof(char) * (log(decimal)/log(2))) + 2);
	char* binary = malloc(len);
	int i;
	// converts the binary number
	for(i = 0; decimal > 0; i++)
	{
		// inserts values backward
		if(decimal % 2 != 0)
		{
			binary[len - 2 - i] = '1';	
		}
		else
		{
			binary[len - 2 - i] = '0';
		}
		decimal = decimal / 2;
	}
	
	// prints the binary number
	binary[len - 1] = '\0';
	printf("Conversion:\t%s\n", binary);
	free(binary);
}

/**
**checks to see if the input contains any alphabetical characters
**/
bool checkalpha(char* line, long* number)
{
	long n;
	char c;
	// if sscanf doesn't return 1
	if(sscanf(line," %lu%c",&n, &c) == 1)
	{
		if(n >= 0)
		{
			*number = n;
			return true;
		}
	}
	
	return false;
}

