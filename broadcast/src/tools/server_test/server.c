#include <stdbool.h>
// FIFO requirements
#include <sys/types.h>
#include <sys/stat.h>
#include <pthread.h>
// Pipe requirements
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>
pthread_mutex_t locker = PTHREAD_MUTEX_INITIALIZER;

int x = 0;
bool terminate = false;

void* traverseThread(void* arg)
{
	pthread_mutex_lock(&locker);
    
   	for(int i = 0; i < 240; i++)
	{
		x = i;
		printf("%d\n", x);
		sleep(1);
		
	}    
	
   terminate = true;
	pthread_mutex_unlock(&locker);
   pthread_exit(0);
}

void* send_header(void* args)
{
    while(terminate == false)
    {	

		signal(SIGPIPE,SIG_IGN);
        // Attempts to open connection and returns INVALID on failure
        int port = open("/tmp/test", O_WRONLY);

        if(port == -1)
        {
            return NULL;
        }
        // sends info to the PHP program
        write(port, &x, 4);
        close(port);
    }

    pthread_exit(0);
}

int main(void)
{
	  // makes sure that the pipe file exists
        mkfifo("/tmp/test", 0666);

        // multiple threading 
        pthread_t tds[2];
        pthread_attr_t attr;
        pthread_attr_init(&attr);
        pthread_attr_t attr1;
        pthread_attr_init(&attr1);
        pthread_create(&tds[0], &attr, traverseThread, NULL);
        pthread_create(&tds[1], &attr1, send_header, NULL);
        pthread_join(tds[0], NULL);
        pthread_join(tds[1], NULL);
}


