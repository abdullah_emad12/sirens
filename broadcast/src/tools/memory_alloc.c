#include<stdio.h>
#include<stdlib.h>
#include<string.h>
typedef char* string;
string getString(void)
{
	int size = 0;
	int capacity = 0;
	char c;
	string buffer = NULL;
	// keeps waiting for input until the user press enter or the EOF is reached
	while((c = fgetc(stdin)) != EOF && c != '\n')
	{
		if(size > capacity - 1)
		{
			if(capacity == 0)
			{
				capacity = 32;
			}
			// doubles the capacity every time we run out of space
			else
			{
				capacity = capacity * 2;
			}
			string temp = realloc(buffer, sizeof(char) * capacity);
			if(temp == NULL)
			{
				free(buffer);
				return NULL;
			}
			buffer = temp;
		}
		buffer[size++] = c;
	}

	// copies the input string to the output string
	string out = malloc((1 + size) * sizeof(char));
	strncpy(out,buffer,size);
	if(buffer != NULL)
	{	
		free(buffer);
	}
	// terminates the string
	out[size] = '\0';
	return out;
}

int main(void)
{
	//char* s = getString();
	/*s[0]='a';
	s[1]='7';
	s[2] ='a';
	s[3] = '\0';*/
	//printf("%s\n",s);
	//free(s);
	// keeps running untill the thread is terminated
	while(1)
	{
		string command = getString();
		// force quit
		if(strcmp(command, "fq") == 0)
		{
			printf("yeah\n");
		}
		// quits normally after the traverse is done
		else if(strcmp(command, "q") == 0)
		{
			printf("\nGot you!\n");
		}
		// prints the total number of requests
		else if(strcmp(command, "r") == 0)
		{
					printf("\nGot you!\n");
		}
		// prints the playlist
		else if(strcmp(command, "l") == 0)
		{
						printf("\nGot you!\n");
		}
		// replays the previous song if any each time
		else if(strcmp(command,"b") == 0)
		{
						printf("\nGot you!\n");
		}
		// skips one song each time
		else if(strcmp(command,"f") == 0)
		{
						printf("\nGot you!\n");
		}
		if(command != NULL)
		{			
			free(command);
		}
	}	
}
